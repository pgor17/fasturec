
Fasturec is a program for inferring a supertree from a set of unrooted gene trees using unrooted reconciliation model. 
It implements a local search algorithm with a number of flexible hill-climbing heuristics. 
Fasturec can be also used to manipulate gene and species trees.

Version 2.06

### Installation ###

You need linux and modern g++ compiler. After downloading run `make`.

To test fasturec with exemplary datasets and several predefined heuristic option execute bash script: `./test_fu.sh`.

### Help ###

To get help run `fasturec` with no options.

### Running basics ###

A standard run (-Z heuristic): 

`fasturec -G datasets/g3.txt -Z`

Starts from the initial tree quasi-consensus tree (default) and executes a hill-climbing steps under the duplication-loss cost.
Quasi-consensus tree is a simple tree reconstructed from cluster frequencies of the input gene trees.
A mixture of NNI, SPR and TSW (leaf swapping) tree edit operations are applied. 

Input file option: -G FILENAME

Output file: <date>.<time>.fu.txt or fu.txt with -ea option.

### Exemplary datasets ###

#### Genolevures 

See `datasets/ac.txt`.

Unrooted gene trees obtained from curated gene families from Treefam v9 (mainly of mammals).

The best tree for DL (duplication-loss cost) has score 7188. 
The best tree for D (duplication cost) has score 2240.

#### Treefam (curated)

See `datasets/g3.txt`.

A set of unrooted gene family trees inferred from Genolevures gene families (quite old).

The best tree for DL (duplication-loss cost) has score 64413.
The best tree for D (duplication cost) has score 15822.


### Examples ###

- A simple usage `fasturec -G g3simple.txt -Y`

- Initial tree is quasi-consensus tree (default), D cost (use -L0 to set loss weight to 0) `fasturec -G g3simple.txt -Y -L0`

- Initial tree is random, DL cost `fasturec -G g3simple.txt -Y -r1` 

- Random initial tree, D cost: `fasturec -G g3simple.txt -Y -r1 -L0`

#### Random vs quasi-consensus initial trees ####

Running from random initial tree may lead to non-optimal solution. E.g. compare (run several times):
`fasturec -G ac.txt -Y  -r1` vs `fasturec -G ac.txt -Y`

#### Tree space exploration ####

To explore more deeply the space, add, for example, -j10 parameter:

fasturec -G ac.txt -Y  -r1 -j10

#### Alternatives  ####

-r option can be used with larger numbers. However, I'm not fully happy with this approach.
Try (10 random trees, and with 10 repeats):  `fasturec -G ac.txt -Y -r10 -k1 -j10`

### Output ###

All trees explored during fasturec are saved to a file: `<date>.<time>.fu.txt` (it might be unsorted). 
Each line of the output file contains cost and a rooted species tree.

To change the number of inferred species trees in the output file use `-c NUM` (default 400).

Write just one tree:
```
./fasturec -G datasets/ac.txt -HMN3S3T3 -q6 -c1
```

### Generic heuristic options ###

- -Z - described above
- -M - a variant of -Z
- -Y - star-tree greedy heuristic; uses NNI and SPRn for improving costs in each step

See below to a more detailed specification of these methods.

### Defining user heuristic searches ###

##### Option -H #####
Use `-H (N[NUM]|S[NUM]|T[NUM]|M)+` to define local search algorithm, where

- N[NUM] - NNI local search
- S[NUM] - SPR local search
- T[NUM] - subtree swapping local search
- M - use multiple queue method (one queue per each starting tree)
     
NUM in N,S,T denotes the number of the corresponding single search types in every loop
  lack of NUM denotes searching until no improvement is found

##### Specification of generic heuristic options #####

* -Y - star heuristic (uses NNI and SPRn for improving costs)

* -Z - equivalent to -H MNS5T5

* -M - equivalent to -H NS1NS1NS1NS1T5

### Rooting the species tree

The optimization based on unrooted gene trees may shift the root of the inferred species to incorrect position. We recommend either to ignore the root or to fix the root by setting the position using -er (not available in star heuristic) or -er0.

The root split is preserved at (YALI,DEHA) and the rest. 
```
./fasturec -G datasets/g3.txt -Z -er -s "((((ERGO,(KLTH,SAKL)),KLLA),(ZYRO,(CAGL,SACE))),(YALI,DEHA))"
```

Since, all trees from `-s` are initial species trees in the heuristic, try also `-er0`, to remove the first species tree from the set of initial species trees.


### Branch lengths

Branch lengths and supports can be present but fasturec ignores them.

### Cite ###

Fasturec algorithm is based on the following papers:

Górecki et al. GTP Supertrees from Unrooted Gene Trees: Linear Time Algorithms for NNI Based Local Searches, LNCS, 2012, 
https://doi.org/10.1007/978-3-642-30191-9_11

Górecki, Tiuryn, Inferring phylogeny from whole genomes, Bioinformatics, 2007, 
https://doi.org/10.1093/bioinformatics/btl296

Górecki, Tiuryn, URec: a system for unrooted reconciliation, Bioinformatics, 2006, 
https://doi.org/10.1093/bioinformatics/btl634

