/************************************************************************
 Unrooted REConciliation version 2.06
 (c) Copyright 2005-2021 by Pawel Gorecki
 Written by P.Gorecki.
 Permission is granted to copy and use this program provided no fee is
 charged for it and provided that this copyright notice is not removed.
 *************************************************************************/

const char *FASTUREC="2.06";

// 2.01: Preserve root (-er), args -e ....
// 2.02: args -H heuristic alg. specification
// 2.03: option -s corrected; stvec processing is first
// 2.04: lfnum corrected
// 2.05: -w corrected
// 2.06: dirmap error corrected


#include <time.h> 
#include <sys/time.h>

//#define NNIDEBUG
//#define NNIDEBUGREC
//#define ARGSLOG
//#define DEBUGREC3
//#define NNIDEBUGTREEREPR
//#define GSESUBTREES
//#define NNIVERIFY
//#define NNIDUMP
//#define DEBUGCONS
//#define SPRDEBUG
//#define SPRDEBUG2
//#define SPRNDEBUG
//#define TSWDEBUG

#define TRNEW 0
#define TRPROCESSED 1

#define tsprocstats if (ls.tproc%25==0) { verb_smp << "."; fflush(stdout); }

#define COSTDL 0   // dl
#define COSTD 1  // duplication
#define COSTS 2  // speciation
#define COSTTEST 100 // test 

#define COSTGTC 2
#define COSTGTS 1
#define COSTDC 10
#define COSTDCX 11
#define COSTRF 20
#define COSTRFSPEC 21
#define COSTRFO 22
#define COSTRFFD 23
#define COSTL 30

#define ARR_DATE 1
#define ARR_COST 2
#define ARR_SIMPLE 3


int _gsetrees=0;

int savetabstyle = ARR_COST;
int TAB_MAXSIZE=400;

// The max number of trees in the output file
int inferredspeciesrteescount = TAB_MAXSIZE;

int moutproblem = 0;

// rooted tree: n leaves, n-1 int, 2n-1 total
// unrooted tree: n leaves, 3(n-2) int, 4n-6 total

// general n max:  species n/2-2, n/4
// char: max species: 127, max gene leaves: 64 leaves

#define SPC int

typedef double COSTTYPE;

#define LCATAB

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

#include <string.h>
#include <iostream>
#include <set>
#include <vector>
#include <algorithm>
using namespace std;

#include "tools.h"

#include <sstream>
#include <queue>


// #define pcc(d,v) printf("\nDEBUGGER%d: " #v "=%g\n",d,v);
// #define pcct(d,v) printf("\nDEBUGGER%d: " #v "=%g\n",d,v.cost);

#define pcc(d,v)
#define pcct(d,v)

int nnirandomtrees = 0;
int nniconstrees = 0;
double nniconsprob = 0.2;
int mixexplorebigsteps = 2;

char *projectname = strdup("byopt");

#define VERBOSE_QUIET 1
#define VERBOSE_CNTCOST 2
#define VERBOSE_SMP 3
#define VERBOSE_DETAILED 4

#define verb_cntcost if (verbose==VERBOSE_CNTCOST) cout
#define verb_smp if (verbose>=VERBOSE_SMP) cout
#define verb_det if (verbose>=VERBOSE_DETAILED) cout

#define deletespcluster(s) if (s!=topspcluster && ((s)[0]!=1)) delete[] s;

int verbose = VERBOSE_SMP; 

template <class T>
inline std::string to_string (const T& t)
{
  std::stringstream ss;
  ss << t;
  return ss.str();
}

double weight_loss = 1.0;
double weight_dup = 1.0;

int gsid = GSFULL;
char *gsdelim = NULL;
int gspos = 0;

int lfnum(char *str)
{
  int alprev = 0;
  int cnt = 0;
  char *s=str;

  cnt=1;
  while (*str) 
  {
    if (*str == ',') cnt++;
    str++;
  }
  return cnt;

}

#define LEAFLOOP for (SPID i=0; i<lf; i++)
#define INTERNALNODESLOOP for (SPID i=lf; i<nn; i++)
#define NO for (SPID i=0; i<nn; i++)
#define leftchild(i) child[(i)-lf]
#define rightchild(i) child[(i)]
#define isinternalnode(i) ((i)>=lf)
#define isleaf(i) ((i)<lf)

#define DEFBL -100.0   // def. branch length in a gene tree

typedef struct DlCost
{
  long dup;
  long loss;
  DlCost(long dl = 0, long ls = 0) : dup(dl), loss(ls) {}
  friend ostream& operator<<(ostream&s, DlCost p)
  { return s << "(" << p.dup << "," << p.loss << ")"; }
  friend DlCost operator+(DlCost s1, DlCost s) { return DlCost(s1.dup + s.dup, s1.loss + s.loss); }
  COSTTYPE mut() { return 1.0 * weight_dup * dup + 1.0 * weight_loss * loss; }
  int smp() { return dup + loss; }
} DlCost;


typedef struct TotalCost
{
  long dup;
  long loss;
  double cost;
  TotalCost(long d, long l, COSTTYPE m=0) : dup(d), loss(l), cost(m) {}
  friend ostream& operator<<(ostream&s, TotalCost p)
  { return s << "(" << p.dup << "," << p.loss << ")"; }
  friend TotalCost operator+(TotalCost s1, TotalCost s) 
  {  return TotalCost(s1.dup + s.dup, s1.loss + s.loss, s1.cost+s.cost); }  
  
  void clear() { dup=0; loss=0; cost=0; }
  // void update(DlCost &c) { sdup+=c.getdup(); sloss+=c.getloss(); 
  //   cost+=c.cost(); }
  void update(DlCost &c) { dup+=c.dup; loss+=c.loss; cost+=c.mut(); }  
  void update(TotalCost &c) { dup+=c.dup; loss+=c.loss; cost+=c.cost; }  
} TotalCost;



class UnrootedTree;
class RootedTree;

typedef vector<UnrootedTree*> GTS;
typedef vector<RootedTree*> STS;

struct comparespids
{
  bool operator()(const SPID* a, const SPID* b) const
{
      int i = 1, j = 1, al = a[0] + 1, bl = b[0] + 1;


      if (al<bl) { //cout << "Tx";
	return true; }
      if (al>bl) { //cout << "Fx";
	return false; }
    for ( ; i < al; i++ ) {
      if (a[i] < b[i]) { //cout << "T";
	return true;}
      if (a[i] > b[i]) { //cout << "F";
	return false; }
    }
      //    cout <<"F";
    return false;
	
  }
};

typedef struct GTCluster
{
  GTCluster *l, *r; // child nodes composition; needed to compute lca
  SPID type;        // >=0 species id, -1 - all species, -2 - l-r
  SPID lcamap;      // map to species tree
  SPID* spcluster;  // cluster
  int usagecnt;     
  
  GTCluster(GTCluster *lc, GTCluster *rc, SPID *s) : l(lc), r(rc), type(-2), lcamap(-1), spcluster(s), usagecnt(0)  
  { if (s[0] == 1) type = s[1]; }

  friend ostream& operator<<(ostream&s, GTCluster &p) {
    printspcluster(s, p.spcluster);
    return s;
  }

  ~GTCluster()
  {
    deletespcluster(spcluster);
  }

} GTCluster;

typedef GTCluster* GTClusterArr;

class TreeClusters
{
  int ssize;
  int _usagecnt;
  map<SPID*, GTCluster*, comparespids> t;
  vector<GTCluster*> internal;
  vector<GTCluster*> leaves;
public:
  TreeClusters()
  {
    ssize = specnames.size();
    _usagecnt = 0;
    // initialize spec id clusters

    for (SPID i = 0; i < ssize; i++)
    {
      GTCluster *gc = new GTCluster(0, 0, spec2spcluster[i]);
      t[gc->spcluster] = gc;
      leaves.push_back(gc);
    }
  } 

  ~TreeClusters()
  {
    for (SPID i = 0; i < ssize; i++)
        delete leaves[i];

    for (SPID i = 0; i < internal.size(); i++)
        delete internal[i];      
  }

  void addgenetrees(GTS &gt);
  
  GTCluster* leafcluster(SPID n) { leaves[n]->usagecnt++; _usagecnt++; return leaves[n]; }

  GTCluster* get(GTCluster *l, GTCluster *r);
  int size() { return t.size(); }
  int usagecnt() { return _usagecnt; }
  void print()
  {
    map<SPID*, GTCluster*, comparespids>::iterator it;    
  }
  void setlcamappings(RootedTree &s);
  SPID _setmaps(GTCluster *gc, RootedTree &s);

  RootedTree* genrooted(RootedTree *preserveroottree);

};

TreeClusters *gtc;

typedef set<SPID> cluster;
typedef map<cluster, int> cntmap;

bool compcnt(const GTCluster *a, const GTCluster *b)
  { return a->usagecnt > b->usagecnt; } 

bool compsize(const GTCluster *a, const GTCluster *b)
  { return (spsize(a->spcluster) < spsize(b->spcluster)); } 

class GTCC {
public:
  SPID *c;
  string s;
  int clean;
  GTCC(SPID *_c, string _s, bool _clean=0) : c(_c), s(_s), clean(_clean) {}
};



class UnrootedTree;
class RootedTree;


////////////////////////////////////////////////////////
// generic tree
class Tree
{
protected:

  void init(int _lf, int _it, char *s = NULL)
  {
    lf = _lf;
    it = _it;
    nn = lf + it;
    if (lf > MAXSP)
    {
      cout << "Tree is too large. " << lf << " leaves." << endl;
      cout << "Compile with SPLARGE or SPMED macro." << endl;
      exit(-1);
    }
 
    if (nn>0) par = new SPID[nn];
    else par=NULL;
    lab = new SPID[lf];
    
  }



  virtual ostream& _printdebnode(ostream&s, SPID i)
  {

    s << setw(3) << (SPC)i;
    if (par[i] == MAXSP) s << "-^-";
    else s << setw(3) << (SPC)par[i];

    if (i < lf)
      s << " $" << (SPC)lab[i] << " " << species(lab[i]);
    else s << " *   ";
    return s;
  }


public: // GenericTree
  string name;
  double weight;
  int lf;
  int it;
  int nn;
  SPID *par;
  SPID *lab;
  

  ~Tree()
  {
    if (par) delete[] par;
    delete[] lab;
  }

  virtual ostream& printdebnode(ostream&s, SPID i)
  {
    s << setiosflags(ios::left);
    _printdebnode(s, i) << resetiosflags(ios::left);
    return s;
  }

  virtual ostream& printsubtree(ostream&s, SPID i) { return s; }

  Tree(double treeweight = 1.0) : name(""), weight(treeweight), lf(0), it(0), nn(0)
  {}


  // Tree
  virtual ostream& printdeb(ostream&s, int gse, string tn="") {
    if (gse&2)
    {
    s << "leaves=" << lf << " internal=" << it << " total=" << nn << endl;
    NO printdebnode(s, i) << endl;    
    }
    if (gse&1)
    {
      s << "&s (";
      char cnt='A';
      NO if (par[i]==MAXSP)
      {    
           if (cnt!='A') s << ",";          
           printsubtree(s,i) << " edgelab=\"" << cnt << "\"" ;
           cnt++;
      }
      s << ")" << " treename=\"" << _gsetrees++ << " " << tn << "\"" ;
    }
    return s;
  }

  virtual ostream& print(ostream&s)  {
    s << lf << it << nn << endl;
    NO printdebnode(s, i) << endl;
    return s;
  }
}; // Tree


#define MUT 1
#define MUX 2
#define MUSPEC 4
#define MUDUP 8

#define MU2X 16
#define MUA 32
#define MUB 64

#define MUTDUP 9
#define MUTSPEC 5
#define MUXDUP 10
#define MUXSPEC 6

int dumpcnt = 0;

class TreeRepr;


////////////////////////////////////////////////////////
// rooted binary tree
class RootedTree : public Tree
{
protected:

  void init(int _lf, int _it, char *s = NULL)
  {
    Tree::init(_lf, _it, s);
    child = new SPID[nn];
    level = new SPID[nn];
    depth = NULL;
  }

  void _setdepth(SPID i, int dpt)
  {
    depth[i] = dpt;
    if (isleaf(i)) return;
    _setdepth(leftchild(i), dpt + 1);
    _setdepth(rightchild(i), dpt + 1);
  }

  SPID parse(char *s, int &p, int num, SPID &freeleaf, SPID &freeint)
  {
    char *cur = getTok(s, p, num);

    if (cur[0] == '(')
    {
      int a = parse(s, p, num, freeleaf, freeint);
      getTok(s, p, num);
      int b = parse(s, p, num, freeleaf, freeint);
      getTok(s, p, num);
      if (freeint >= nn)
      {
        cerr << "Is it a binary tree? Too many nodes in rooted tree" << endl;
        exit(-1);
      }
      child[freeint] = b;
      child[freeint - lf] = a;
      par[a] = freeint;
      par[b] = freeint;
      level[freeint] = max(level[a], level[b]) + 1;
      return freeint++;
    }
    // leaf
    if (freeleaf >= lf)
    {
      cerr << "Is it a binary tree? Too many leaves in rooted tree" << endl;
      exit(-1);
    }
    lab[freeleaf] = getspecies(cur, s + p - cur);
    level[freeleaf] = 0;
    return freeleaf++;
  }

  // RootedTree
  virtual ostream& _printdebnode(ostream&s, SPID i)
  {
    Tree::_printdebnode(s, i);
    if (root == i) s << "R"; else s << " ";
    if (isinternalnode(i))
      s << " " << setw(3) << (SPC)leftchild(i) << " " << setw(3) << (SPC)rightchild(i);
    if (mdup)
      s << " gd=" << setw(3) << (SPC)mdup[0][i];
    if (mspec)
      s << " gs=" << setw(3) << (SPC)mspec[0][i];
    printsubtree(s, i);
    return s;
  }

  void setlevel(int i)
  {
    if (isleaf(i)) level[i] = 0;
    else
    {
      setlevel(leftchild(i));
      setlevel(rightchild(i));
      level[i] = 1 + max(level[leftchild(i)], level[rightchild(i)]);
    }
  }

public :
  SPID root, *level, *child, *depth;
  SPID **mdup, **mspec;  // double arrays: gene -> loss
  SPID **mout;   // the number of outgoing lineages for a given gt and a sp. node
  int exactspecies; // true iff for each leaf lab[leaf]==leaf
  char *mapupdate; // sp-nni only

#ifdef LCATAB
  SPID **lcatab;
#endif

  RootedTree(char *s, double weight = 1.0) : Tree(weight)
  {
    if (strstr(s, ".raw"))
    {
      // open raw file
      ifstream sn;
      sn.open(s);
      int sz;
      char buf[100];
      sn >> sz;
      for (int i = 0; i < sz; i++)
      {
        sn >> buf;
        getspecies(buf, strlen(buf));
      }
      sn >> nn;
      sn >> lf;
      init(lf, lf - 1, s);
      for (int i = 0; i < nn; i++) sn >> par[i];
      for (int i = 0; i < lf; i++) sn >> lab[i];
      for (int i = 0; i < nn; i++) sn >> child[i];
      sn >> root;
      setlevel(root);
      sn.close();
    }
    else
    {
      int lfn = lfnum(s);
      init(lfn, lfn - 1, s);
      int p = 0;
      SPID freeleaf = 0;
      SPID freeint = lf;
      while (s[0]>='0' && s[0]<='9') s++; // skip optional first number
      root = parse(s, p, 0, freeleaf, freeint);
      checkparsing(s + p);
    }
    exactspecies = 0;
    LEAFLOOP { if (lab[i] != i) exactspecies = 0; break; }
    depth = NULL;
#ifdef LCATAB
  lcatab = NULL;
#endif
  mdup = NULL;
  mspec = NULL;
  par[root] = MAXSP;
  depth = new SPID[nn];
  }

  void subtreenodes(SPID i, SPID t[], int &cnt)
  {
    t[cnt++] = i;
    if (isleaf(i)) return;
    subtreenodes(leftchild(i), t, cnt);
    subtreenodes(rightchild(i), t, cnt);
  }

  void rightsubtrecandidates(SPID i, SPID t[], int &cnt, int preserveroot)
  {
    while (i!=root)
    {   
        if (i==leftchild(par[i])) 
          if (!preserveroot or preserveroot and par[i]!=root)
            subtreenodes(rightchild(par[i]),t,cnt);
        i=par[i];
    }
  }

  void swapchildren(SPID n)
  {
        SPID tmp=leftchild(n);
        leftchild(n)=rightchild(n);
        rightchild(n)=tmp;
  }

  int isleftchild(SPID n)
  {
      return n==leftchild(par[n]);
  }


  // Detach subtree rooted at i; freenode is a parent of i
  // Return 1 iff i is the root
  int sprdetach(SPID i, SPID &sib)
  {
    if (i == root) return 0;
    sib = sibling(i);
    SPID freenode = par[i];
    if (freenode == root)
    {
      // detaching child of the root
      root = sib;
      par[sib] = par[freenode];
    }
    else
    {
      SPID ppi = par[sib] = par[freenode];
      par[freenode] = MAXSP;
      if (leftchild(ppi) == freenode) leftchild(ppi) = sib;
      else rightchild(ppi) = sib;
    }
    if (leftchild(freenode) == sib) leftchild(freenode) = MAXSP;
    else rightchild(freenode) = MAXSP;

    return 1;
  }


  int swapnodes(SPID a, SPID b)
  {
    if (a==root || b==root) return 0;
    if (a==sibling(b)) return 0;
    SPID pa=par[a];
    SPID pb=par[b];
    par[a]=pb;
    par[b]=pa;
    
    if (leftchild(pa) == a) leftchild(pa) = b;    
    else rightchild(pa) = b;
    if (leftchild(pb) == b) leftchild(pb) = a;    
    else rightchild(pb) = a;
    return 1;
  }

  int sprattach(SPID src, SPID dest)
  {
    SPID pari = par[src];
    if (dest == root) {
      SPID prevroot = root;
      root = pari;
      par[prevroot] = root;
      par[src] = root;
      par[root] = MAXSP;
      if (leftchild(root) == src) rightchild(root) = prevroot;
      else leftchild(root) = prevroot;
      return 1;
    }

    SPID ppi = par[dest];
    par[pari] = ppi;
    par[dest] = pari;

    if (leftchild(ppi) == dest)
      leftchild(ppi) = pari;
    else rightchild(ppi) = pari;

    if (leftchild(pari) == src) rightchild(pari) = dest;
    else leftchild(pari) = dest;

    return 1;
  }

  SPID sibling(SPID i)
  {
    if (i == root) return -1;
    SPID p = par[i];
    if (i == leftchild(p)) return rightchild(p);
    return leftchild(p);
  }

  // Replace old tree with a new one. Size must be the same
  void replace(TreeRepr *tr);

  virtual ostream& print(ostream&s) 
  {
    printsubtree(s, root);
    return s;
  }

  virtual void getchildren(SPID i, SPID &l, SPID &r)
  {
    l = leftchild(i);
    r = rightchild(i);
  }


  virtual int subtreesize(SPID n)
  {
    if (isleaf(n)) return 1;
    return subtreesize(leftchild(n))+subtreesize(rightchild(n));
  }

  SPID** getspclusterrepr()
  {
        SPID **res=new SPID*[nn];
        LEAFLOOP res[i]=spec2spcluster[lab[i]];
        INTERNALNODESLOOP res[i]=joinspclusters(res[leftchild(i)],res[rightchild(i)]);
        return res;
  }

  void saveraw(string fn = "st.raw")
  {
    ofstream s;
    s.open(fn.c_str());
    s << specnames.size() << endl;
    for (int i = 0; i < (int)specnames.size(); i++) s << species(i) << " ";
    s << endl;
    s << nn << " " << lf << " ";
    s << endl;
    for (int i = 0; i < nn; i++) s << par[i] << " ";    s << endl;
    for (int i = 0; i < lf; i++) s << lab[i] << " ";     s << endl;
    for (int i = 0; i < nn; i++) s << child[i] << " ";     s << endl;
    s << root;
    s << endl;
    s.close();
  }

  void dumpnnistructs(GTS &g, int gtree = -1, int num = -1); // for comparing DEBUGs

  SPID _outdlcost(SPID node, SPID *duplen, SPID *speclen, DlCost &c);
  void outdlcost(SPID maptau, SPID *duplen, SPID *speclen, DlCost &c, SPID LGsize);
  void checkoutdlcost(SPID maptau, SPID *duplen, SPID *speclen, DlCost &c, SPID LGsize);

  void _nniprint(string s, UnrootedTree &g, int gtnum = 0, ostream &c = cout);
  SPID _outdistribution(SPID node, SPID *dups, SPID *specs, SPID *mout);
  SPID _checkoutdistribution(SPID node, SPID *dups, SPID *specs, SPID *mout, int &ok);

  //Rooted tree, ok if
  SPID clustersize(SPID n)
  {
    if (isleaf(n)) return 1;
    return clustersize(leftchild(n)) + clustersize(rightchild(n));
  }

  int _lca(SPID a, SPID b)
  {
    int lev = 0;
    while (1)
    {
      if (a == b) return a;
      if (a == root) return root;
      if (b == root) return root;
      if (level[a] == lev) a = par[a];
      if (level[b] == lev) b = par[b];
      lev++;
    }
  }

  friend ostream& operator<<(ostream&s, RootedTree &p)  
  { 
    return p.print(s); 
  }

  void initlca()
  {
    setlevel(root);
#ifdef LCATAB
    initlcatab();
#endif
  }

#ifdef LCATAB
  void initlcatab()
  {
    if (!lcatab) {
      lcatab = new SPID*[nn];
      for (int i = 1; i < nn; i++) lcatab[i] = new SPID[i];
    }
    for (int i = 1; i < nn; i++)
      for (int j = 0; j < i; j++)
        lcatab[i][j] = _lca(i, j);
  }
#endif

  int lca(SPID a, SPID b)
  {
#ifdef LCATAB
    if (a == b) return a;
    if (!lcatab) initlca();
    if (a < b) return lcatab[b][a];
    return lcatab[a][b];
#else
    return _lca(a, b);
#endif
  }

  virtual ostream&  printsubtree(ostream &s, SPID i)
  {

#ifdef GSESUBTREES

    if (i == MAXSP) s << "---";
    else    
    if (isleaf(i)) s << species(lab[i]);
    else
    {
        s << "(";
        printsubtree(s, leftchild(i)) << ",";
        printsubtree(s, rightchild(i)) << ")";
    }
    return s<< " nodeinfo=\"" << i << "\" edgelab=\"" << par[i] << "\"";

#else
 if (i == MAXSP) return s << "---";
    if (isleaf(i)) return s << species(lab[i]);
    s << "(";
    printsubtree(s, leftchild(i)) << ",";
    return printsubtree(s, rightchild(i)) << ")";
#endif
  }

  // Requirement: a <= b
  inline virtual SPID dist(SPID a, SPID b)
  {
    return depth[a]-depth[b];
    //int cnt = 0;
    //while (a != b) { a = par[a]; cnt++; }
    //return cnt;
  }

  inline virtual SPID nodedist(SPID a, SPID b)
  {
    SPID lcaab=lca(a,b);
    return dist(a,lcaab)+dist(b,lcaab);
  }

  virtual void _repr(SPID *t, SPID i, int &pos, SPID* mm)
  {
    if (isleaf(i))
    {
      t[pos++] = lab[i];
      return;
    }
    t[pos++] = MAXSP; // (

    if (mm[leftchild(i)] == mm[i])
    {
      _repr(t, leftchild(i), pos, mm);
      t[pos - 1] = -t[pos - 1]; //,
      _repr(t, rightchild(i), pos, mm);
    }
    else
    {
      _repr(t, rightchild(i), pos, mm);
      t[pos - 1] = -t[pos - 1]; //,
      _repr(t, leftchild(i), pos, mm);
    }
    t[pos++] = MAXSP + 1; // )
  }

  virtual SPID _setmm(SPID i, SPID* mm)
  {
    if (isleaf(i)) mm[i] = lab[i];
    else
    {
      SPID lm = _setmm(leftchild(i), mm);
      SPID rm = _setmm(rightchild(i), mm);
      mm[i] = min(lm, rm);
    }
    return mm[i];
  }

  int reprlen() {
    return  lf * 3 - 2;
  }

  virtual SPID* repr(SPID *t = NULL)
  {
    if (!t) t = new SPID[reprlen()];
    SPID mm[nn];
    _setmm(root, mm);
    int cpos = 0;
    _repr(t, root, cpos, mm);
    return t;
  }

  SPID findlab(SPID slab, int stoponerr=1)
  {
    if (exactspecies) return slab;
    LEAFLOOP if (slab == lab[i]) return i;
    if (stoponerr)
    {
      cerr << "Species lab " << specnames[slab] << " (" << slab << ") not found in the tree: ";
      print(cerr);
      cerr << endl;
      exit(-1);
    }
    return MAXSP;
  }



  void setdepth(SPID i = -1, SPID dpt = 0)
  {    
    _setdepth(root, 0);
  }

  int getlosses(SPID s, SPID s1, SPID s2)
  {
    if (s != s2)
    {
      if (s == s1)
      {
        return (int)depth[s2] - (int)depth[s];
      }
      else
      {
        return (int)depth[s1] + (int)depth[s2] - 2 * (int)depth[s] - 2;
      }
    }
    return (int)depth[s1] - (int)depth[s];
  }

  void nniinit(GTS &g, int create = 1);
  void nnireinit(GTS &g);

  int nni(SPID t, int left, GTS &g, TotalCost &cost, int &xcase, string &info);
  int nnirot(SPID t, GTS &g, TotalCost &cost, int &xcase, string &info, SPID artroot);  

  SPID _nniupdate(UnrootedTree *g, SPID gn, SPID t, SPID x);

  int isparent(SPID pr, SPID n, SPID upper)
  {
    // used only in nni's where lca's are invalid
    // todo: replace with sth more efficient
    // bitarrays
    //cout << pr << " " << n << endl;
    do {
      //cout << pr << " " << n << endl;
      if (pr == n) return 1;
      if (n == root) return 0;
      n = par[n];
    } while (n != upper);
    return 0;
  }

  // initialize computations for nni's
  TotalCost initGTS(GTS &gt, int first, int initnnistructs = 1);

  TotalCost gtccost(GTS &gt);


  SPID getparent(SPID n) { return par[n]; }



}; // RootedTree
////////////////////////////////////////////////////////

int offset[3][2] = { { 1, 2}, {1, -1}, { -2, -1}};
//#define lc3(i)  ((i)+offset[(((i)-lf)%3)][0]])
//#define rc3(i)  ((i)+offset[(((i)-lf)%3)][1]])

#define off(i) int *_off = offset[((i)-lf)%3]; 
#define lc3(i)  (par[(i)+_off[0]])
#define rc3(i)  (par[(i)+_off[1]])

#define l3(i) ((i)+_off[0])
#define ll3(i) ((i)+_off[1])

long decnt=0; 

////////////////////////////////////////////////////////
// unrooted binary tree
class UnrootedTree : public Tree
{
protected:

  double parseBranchLen(char *s, int &p, int num)
  {
    double branchlen = -10;
    getTok(s, p, num); // eat :
    char *cur = getTok(s, p, num);
    if (sscanf(cur, "%lf", &branchlen) != 1)
    {
      cerr << "Parse error: number expected after :";
      exit(-10);
    }
    return branchlen;
  }

  double checkSupport(char *s, int &p, int num)
  {
    double support = -10;    
    if (s[p]=='.' || isdigit(s[p]))
    {
      char *cur = getTok(s, p, num);
      if (sscanf(cur, "%lf", &support) != 1)
      {
        cerr << "Parse error: support value expected";
        exit(-10);
      }
    }
    return support;
  }

  SPID createNode3(SPID a, SPID b, double branchlength, SPID &freeint)
  {
    len[freeint] = len[a];
    len[freeint + 1] = len[b];
    par[freeint] = a;
    par[freeint + 1] = b;
    par[a] = freeint;
    par[b] = freeint + 1;
    len[freeint + 2] = branchlength;
    freeint += 3;
    return freeint - 1;
  }

  SPID parse(char *s, int &p, int fromroot, int num, int extractspecies, SPID &freeleaf, SPID &freeint) {

    char *cur = getTok(s, p, num);
    double branchlen = DEFBL;
    if (cur[0] == '(') {
      SPID a = parse(s, p, 0, num, extractspecies, freeleaf, freeint );
      getTok(s, p, num);
      SPID b = parse(s, p, 0, num, extractspecies, freeleaf, freeint);
      if (fromroot) {
        if (s[p] == ',') {
          getTok(s, p, num);
          a = createNode3(a, b, DEFBL, freeint);
          b = parse(s, p, 0, num, extractspecies, freeleaf, freeint);
          len[a] = len[b];
        }
        // join a<->b
        par[a] = b;
        par[b] = a;
        if (len[a] == DEFBL) len[a] = len[b];
        else len[b] = len[a];
        if (s[p] != ')') expectTok(")", s, p);
        getTok(s, p, num); // eat )
        checkSupport(s,p,num);        
        if (s[p] == ':') parseBranchLen(s, p, num); // ignored
        return start = b;
      }
      if (s[p] != ')') expectTok(")", s, p);
      getTok(s, p, num); // eat )
      checkSupport(s,p,num);              
      if (s[p] == ':') branchlen = parseBranchLen(s, p, num);
      return createNode3(a, b, branchlen, freeint);
    }

    //start = createLeaf(cur, s + p - cur);
    int spid = -1;
    char *fullname = NULL;
    int clen = s + p - cur;

    if (extractspecies)
    {
      fullname = mstrndup(cur, clen);
      switch (gsid) {
      case GSPOS:
        if ((gspos >= clen) || (clen + gspos <= 0)) spid = getspecies(cur, clen);
        else if (gspos > 0) spid = getspecies(cur, gspos);
        else spid = getspecies(cur + (clen + gspos), -gspos);
        break;
      case GSAFTER:
      {
        char *st = strstr(fullname, gsdelim);
        if (!st)
        {
          cerr << "No species name found in " << fullname << endl;
          exit(-1);
        }
        st = st + strlen(gsdelim);
        //cout << "st=" << st << endl;
        char *ste = strstr(st, gsdelim);
        if (!ste)
          spid = getspecies(st, strlen(st));
        else
          spid = getspecies(st, ste - st);
      }
      case GSBEFORE:
      {
        char *st = strstr(fullname, gsdelim);
        if (!st)
        {
          cerr << "No species name found in " << fullname << endl;
          exit(-1);
        }
        spid = getspecies(cur, st - fullname);
      }
      break;
      }
    }

    if (spid == -1) spid = getspecies(cur, clen);
    lab[freeleaf] = spid;
    //createLeaf(spid,fullname);
    if (s[p] == ':')
      len[freeleaf] = parseBranchLen(s, p, num);
    if (gsid != GSFULL)
      fullnames[freeleaf] = fullname;
    else 
      if (len) len[freeleaf] = DEFBL;
    return freeleaf++;
  } // parse

  // unrooted
  virtual ostream& _printdebnode(ostream&s, SPID i)
  {
    Tree::_printdebnode(s, i);
    // s << " l=" << len[i];
    if (map) s << " map=" << setw(3) << (SPC)map[i];
    //if (dup) s << " dup=" << setw(3) << dup[i];
    //if (loss) s <<" lss=" << setw(3) << loss[i];
    printsubtree(s, i);
    s << "===";
    printsubtree(s, par[i]);

    return s;
  }

public:

  double *len; // branchlengths
  char **fullnames; // fullnames (only if used)
  SPID start;
  SPID *map, maptau; // the root of the mappings
  int *dup, *loss;
  SPID *mdup, *mspec; // nni only
  SPID *cost; // some cost
  DlCost nnicost; // nni only
  GTCluster **gtcclusters;
  GTCluster *taugtccluster;
  int fixedduplications;

  UnrootedTree(char *s, int _fixedduplications=0, double weight = 1.0) : Tree(weight)
  {
    
    int lfn = lfnum(s);
    gtcclusters=NULL;
    fixedduplications=_fixedduplications;
    init(lfn, 3 * (lfn - 2), s);
    if (nn>0)
        len = new double[nn];
    else len=NULL;
    int p = 0;
    SPID freeleaf = 0;
    SPID freeint = lf;
    if (gsid != GSFULL) fullnames = new char*[lf];
    else fullnames = NULL;
    start = parse(s, p, 1, 0, gsid != GSFULL, freeleaf, freeint);
    checkparsing(s + p);
    dup = loss = NULL;
    map = NULL;
    cost = NULL;
  }

  ~UnrootedTree()
  {
    if (nn) delete[] len;
    if (gtcclusters) {       
      delete[] gtcclusters;
    }
    if (dup) 
    {
      delete[] dup;
      delete[] loss;
    }
  }


  // sp mapping directed parent
  SPID dirmapparent(SPID node)
  {
    
    // cout << "[" << decnt++ << "] DP: node=" << node << " ";
    // cout << " maptau=" << maptau << " ";
    
    SPID parent = par[node];
    
    // cout <<  " par[node]=" << parent << " "; // 1 
    // cout <<  " lf=" << lf << " "; 

    if (parent<lf) 
      return map[parent]; // leaf map

    off(parent);     

    SPID c = l3(parent);  
    
    // cout << " c1=" << c << " "; 
    
    if (map[c] != maptau)       
      return map[c]; 
        
    c = ll3(parent);
    
    //  cout << " c2=" << c << " ";
    if (map[c] != maptau) 
      return map[c]; 
    
    //cout << "-1 opt";
    return -1;
  }



  virtual ostream& print(ostream&s) 
  { 
    if (nn<1) { 
        s << species(lab[0]);
        return s;
    }
    return printsubtree(s, start, 1); 
  }
  
  virtual ostream& printrooting(ostream&s, SPID n) { return printsubtree(s, n, 1); }

  virtual void getchildren(SPID i, SPID &l, SPID &r)
  {
    off(i);
    l = lc3(i);
    r = rc3(i);
  }

  virtual SPID _setmaps(RootedTree &s, SPID i)
  {
    if (map[i] == -1)
    {
      off(i);
      map[i] = s.lca(_setmaps(s, lc3(i)), _setmaps(s, rc3(i)));
    }
    return map[i];
  }

  virtual ostream&  printsubtree(ostream &s, SPID i, int fromp = 0)
  {
    // @todo: print gene names
    if (fromp)
    {
      s << "(";
      printsubtree(s, i) << ",";
      return printsubtree(s, par[i]) << ")";
    }
    if (isleaf(i)) return s << species(lab[i]);
    s << "(";
    off(i);
    printsubtree(s, lc3(i)) << ",";
    return printsubtree(s, rc3(i)) << ")";
  }

  virtual string contracted(SPID i, RootedTree *s, int fromp = 0)
  { 
    string rs,ls;   
    if (fromp)
    {
      
      ls=contracted(i,s);
      rs=contracted(par[i],s);    
    }
    else 
    {
      if (isleaf(i)) {
        if (s->findlab(lab[i],0)!=MAXSP) return species(lab[i]);
        else return string(""); }
        
      off(i);
      ls=contracted(lc3(i),s);
      rs=contracted(rc3(i),s);
    }

    if (ls.empty()) return rs;
    if (rs.empty()) return ls;
    return string("("+ls+","+rs+")");    
  }



  virtual void linfo(ostream &s, SPID i, int optcost, int rev = 1)
  {
    if (map[i] == maptau)
      if (map[par[i]] == maptau) s << " top=2";
      else s << " top=" << rev;
    else if (map[par[i]] == maptau) s << " top=" << -rev;
    else s << " top=0";

    if (cost)
    {
      s << " cost=" << cost[i];
      if (cost[i] == optcost) s << " plateau=" << 1;
      s << " id=" << i;


      if (!isleaf(i))
      {
        off(i);
        if (cost[i] == cost[lc3(i)] && cost[i] == cost[rc3(i)])
          s << " terrace=1";
        else s <<  " terrace=0";

      }
    }

  }

  virtual ostream& printlsubtree(ostream &s, SPID i, int optcost, int ext = -1)
  {
    if (ext >= 0) s << "(";
    if (isleaf(i)) {
      if (gsid != GSFULL)
        if (fullnames[i])
          s << fullnames[i] << " ";
      s << species(lab[i]);
      linfo(s, i, optcost);
    }
    else
    {
      s << "(";
      off(i);
      printlsubtree(s, lc3(i), optcost) << ",";
      printlsubtree(s, rc3(i), optcost);
      s << ")";
      linfo(s, i, optcost);
    }
    if (ext >= 0)
    {
      s << ")";
      linfo(s, ext, optcost, -1);
    }
    return s;
  }

  virtual void mark(SPID node, int val)
  {
    // todo
  }

  virtual void setlcamappings(RootedTree &s)
  {
    if (nn<1) return;
    if (!map) map = new SPID[nn];
    NO map[i] = -1;
    LEAFLOOP map[i] = s.findlab(lab[i]);
    INTERNALNODESLOOP _setmaps(s, i);
    maptau = s.lca(map[0], map[par[0]]);
  }

  // for a different species tree
  virtual void clean(int deletetabs = 0)
  {
#define deltab(t) if (t) { delete[] t; t=NULL; }
    if (deletetabs)
    {
      deltab(map);
      deltab(dup);
      deltab(loss);
    }
    else
    {
      if (map) { NO map[i] = -1; }
      if (dup)
      {
        LEAFLOOP { dup[i] = 0; loss[i] = 0; }
        INTERNALNODESLOOP { dup[i] = -1; loss[i] = -1; }
      }
    }
  }

  virtual void _dlcostsub(RootedTree &s, SPID i)
  {
    if (dup[i] >= 0) return; // computed, including leaves
    // i is internal
    off(i);
    SPID l = lc3(i), r = rc3(i);
    SPID m = map[i];
    SPID ml = map[l];
    SPID mr = map[r];
    _dlcostsub(s, l);
    _dlcostsub(s, r);
    dup[i] = dup[l] + dup[r] + (((m == ml) || (m == mr)) ? 1 : 0);
    loss[i] = loss[l] + loss[r] + s.getlosses(m, ml, mr);
  }

  virtual DlCost dlcost(RootedTree &s, SPID r)
  {
    if (lf==1) return DlCost();    
    if (lf==2) 
        if (lab[0]==lab[1]) return DlCost(1,0);
        else { 
            SPID ml=s.findlab(lab[0]);
            SPID mr=s.findlab(lab[1]);            
            return DlCost(0,s.getlosses(maptau, ml, mr));
        }
    if (!dup)
    {
      dup = new int[nn];
      loss = new int[nn];
      LEAFLOOP { dup[i] = 0; loss[i] = 0; }
      INTERNALNODESLOOP { dup[i] = -1; loss[i] = -1; }
    }
    _dlcostsub(s, r);
    SPID parr = par[r];
    _dlcostsub(s, parr);
    SPID ml = map[parr];
    SPID mr = map[r];
    //    cout << "GT" << s.getlosses(m,ml,mr);
    return DlCost(dup[r] + dup[parr] + (((maptau == ml) || (maptau == mr)) ? 1 : 0), loss[r] + loss[parr] + s.getlosses(maptau, ml, mr));
  }


  virtual void _dlcostsubsmp(RootedTree &s, SPID i, int &du, int &lo)
  {
    if (isleaf(i)) return;
    off(i);
    SPID l = lc3(i), r = rc3(i);
    SPID m = map[i];
    SPID ml = map[l];
    SPID mr = map[r];
    _dlcostsubsmp(s, l, du, lo);
    _dlcostsubsmp(s, r, du, lo);
    du += (((m == ml) || (m == mr)) ? 1 : 0);
    lo += s.getlosses(m, ml, mr);
  }

  virtual DlCost dlcostsmp(RootedTree &s, SPID r)
  {
    if (lf==1) return DlCost();    
    if (lf==2) 
        if (lab[0]==lab[1]) return DlCost(1,0);
        else { 
            SPID ml=s.findlab(lab[0]);
            SPID mr=s.findlab(lab[1]);
            SPID mt=s.lca(ml,mr);
            return DlCost(0,s.getlosses(mt, ml, mr));
        }

    int du = 0, lo = 0;
    _dlcostsubsmp(s, r, du, lo);
    SPID parr = par[r];
    _dlcostsubsmp(s, parr, du, lo);
    SPID ml = map[parr];
    SPID mr = map[r];
    return DlCost(du + (((maptau == ml) || (maptau == mr)) ? 1 : 0), lo + s.getlosses(maptau, ml, mr));
  }


  cluster getcluster(SPID n)
  {
    cluster res;
    if (isleaf(n)) {
      res.insert(lab[n]);
      return res;
    }
    off(n);
    res = getcluster(lc3(n));
    cluster r = getcluster(rc3(n));
    res.insert(r.begin(), r.end());
    return res;
  }


  GTCluster* _getgtccluster(SPID n, TreeClusters *gtc)
  {
    if (gtcclusters[n]) return gtcclusters[n];
    off(n);
    GTCluster *r1 = _getgtccluster(lc3(n), gtc);
    GTCluster *r2 = _getgtccluster(rc3(n), gtc);
    return gtcclusters[n] = gtc->get(r1, r2);
  }

  void setspclusters(TreeClusters *gtc)
  {
    if (nn<1) return;
    gtcclusters = new GTClusterArr[nn];
    NO gtcclusters[i] = NULL;
    LEAFLOOP gtcclusters[i] = gtc->leafcluster(lab[i]);
    INTERNALNODESLOOP _getgtccluster(i, gtc);
    //maptau cluster
    taugtccluster = gtc->get(gtcclusters[0], gtcclusters[par[0]]);
  }

  cluster getrootcluster(SPID n)
  {
    cluster res = getcluster(n);
    cluster r = getcluster(par[n]);
    res.insert(r.begin(), r.end());
    return res;
  }

  SPID clustersize(SPID n)
  {
    if (isleaf(n)) return 1;
    return getcluster(n).size();
  }

  virtual SPID _rfocostsub(RootedTree &st, SPID n)
  {
    if (isleaf(n)) return 0;
    SPID c = 0;
    if (st.clustersize(map[n]) == clustersize(n)) c++;
    off(n);
    return c + _rfocostsub(st, lc3(n)) + _rfocostsub(st, rc3(n));
  }

  virtual SPID rfocost(RootedTree &st, SPID r)
  {
    return _rfocostsub(st, r) + _rfocostsub(st, par[r]) + 0;
  }

  virtual SPID _dccostsub(RootedTree &st, SPID i)
  {
    // @todo SPID type is bad for DC
    if (isleaf(i)) return 0;
    off(i);
    SPID l = lc3(i), r = rc3(i);
    SPID m = map[i];
    SPID ml = map[l];
    SPID mr = map[r];
    return st.dist(ml, m) + st.dist(mr, m) + _dccostsub(st, l) + _dccostsub(st, r);
  }

  virtual SPID _spcostsub(RootedTree &st, SPID i)
  {
    if (isleaf(i)) return 0;
    off(i);
    SPID l = lc3(i), r = rc3(i);
    SPID m = map[i];
    SPID ml = map[l];
    SPID mr = map[r];
    return (((m != ml) && (m != mr)) ? 1 : 0) + _spcostsub(st, l) + _spcostsub(st, r);
  }

  virtual void _rfcostsub(RootedTree &st, SPID n, SPID *cc)
  {
    int cs = clustersize(n);
    if (st.clustersize(map[n]) == cs) cc[map[n]]++;
    if (isleaf(n)) return;
    off(n);
    _rfcostsub(st, lc3(n), cc);
    _rfcostsub(st, rc3(n), cc);
  }

  virtual SPID rfcost(RootedTree &st, SPID r)
  {

    // This formula counts nodes (not clusters) from diff set.
    //cerr << "===========" << endl;
    SPID clu[st.nn];
    for (int i = 0; i < st.nn; i++) clu[i] = 0;
    int c = 2 * lf - 1 + st.nn;
    _rfcostsub(st, r, clu);
    _rfcostsub(st, par[r], clu);
    //printsubtree(cerr,r);    cerr << "===";    printsubtree(cerr,par[r]);    cerr << endl;
    if (st.clustersize(maptau) == (int)getrootcluster(r).size()) clu[maptau]++;
    /// for (int i=0;i<st.nn;i++) if (clu[i]) { st.printsubtree(cerr,i); cerr << " " << i << " " << clu[i] << endl; }
    for (int i = 0; i < st.nn; i++) if (clu[i]) c = c - clu[i] - 1;
    return c;
  }

  virtual SPID dccost(RootedTree &st, SPID r)
  {
    // @todo SPID type is bad for DC
    SPID parr = par[r];
    SPID ml = map[parr]; 
    SPID mr = map[r];
    return st.dist(ml, maptau) + st.dist(mr, maptau) + _dccostsub(st, r) + _dccostsub(st, parr);
  }

  virtual SPID spcost(RootedTree &st, SPID r)
  {
    SPID parr = par[r];
    SPID ml = map[parr];
    SPID mr = map[r];
    return _spcostsub(st, r) + _spcostsub(st, parr) + (((ml != maptau) && (mr != maptau)) ? 1 : 0);
  }

  void pcosts(ostream& s, SPID n, RootedTree *st) {
    DlCost r = dlcost(*st, n);
    s << " totaleftchild({" << r.dup << "," << r.loss << "}) ";
    if (st)
    {
      if (map[n] != maptau)  {
        s << " destn(\"";
        st->printsubtree(s, n);
        s << "\") ";
      }
      else s << " destn(\"\") ";
    }
  }

  SPID _maxdist(SPID n)
  {
    if (isleaf(n)) return 0;
    off(n);
    SPID l = lc3(n), r = rc3(n);
    SPID mxl = _maxdist(l);
    SPID mxr = _maxdist(r);
    if (mxl > mxr) return mxl + 1;
    return mxr + 1;
  }

  void pf(ostream &s, RootedTree &st, int costtype = COSTDL, int linetree = 0) {
    SPID cst[nn];
    int optmin = 1;
    switch (costtype)
    {
    case COSTD:
      NO cst[i] = (SPID)dlcost(st, i).dup;
      break;

    case COSTDCX:
      NO cst[i] = (SPID)dlcost(st, i).loss - 2 * (SPID)dlcost(st, i).dup;
      break;

    case COSTTEST:
      NO cst[i] = (SPID)dlcost(st, i).loss - (SPID)dlcost(st, i).dup;
      break;

    case COSTS:
      optmin = 0;
      NO cst[i] = (SPID)spcost(st, i);
      break;
    case COSTL:
      NO cst[i] = (SPID)dlcost(st, i).loss;
      break;
    case COSTRF:
      NO cst[i] = (SPID)(rfcost(st, i)); // general, based on symetric fifference.
      break;
    case COSTRFO:
      NO cst[i] = (SPID)(st.lf - 2 + lf - 2 - 2 * rfocost(st, i)); // internal-non-root + internal-non-root - 2*non-trivial-equal-speciation, ONLY species trees
      break;
    case COSTRFFD:
      NO cst[i] = (SPID)((rfcost(st, i) - (st.lf - 1) + (lf - 1)) / 2); // FD - non-trivial clusters from G, not present in S
      break;
    case COSTRFSPEC:
      optmin = 0;
      NO cst[i] = (SPID)rfcost(st, i);
      break;
    case COSTDC:
      NO cst[i] = (SPID)dccost(st, i);
      break;
    default:
      NO cst[i] = (SPID)dlcost(st, i).smp();
    }
    // cost = cst;
    cout << "optimalmin=" << optmin << endl;
    int optcost = cst[0];
    NO if (optmin) { if (cst[i] < optcost) optcost = cst[i]; }
    else { if (cst[i] > optcost) optcost = cst[i]; }

    if (!linetree)
    {
      //NO { cout << " " << cost[i] << " "; printdebnode(cout,i); cout << endl;  }
      cout << "(";
      printlsubtree(s, start, optcost);
      cout << ",";
      printlsubtree(s, par[start], optcost);
      cout << ")";
      return;
    }

    // find the longest path ends
    if (lf == 3)
    {
      printlsubtree(s, 0, optcost);
      return;
    }
    SPID dist[lf], n;
    LEAFLOOP dist[i] = 1 + _maxdist(par[i]);
    n = 0;
    LEAFLOOP if (dist[n] < dist[i]) n = i;
    s << "(";
    {
      n = par[n];
      off(n);
      SPID l = lc3(n), r = rc3(n);
      if (isleaf(l)) n = r;
      else n = l;
    }
    printlsubtree(s, par[n], optcost);
    s << ",";
    while (!isleaf(n))
    {
      off(n);
      SPID l = lc3(n);
      SPID r = rc3(n);
      if (isleaf(l) && isleaf(r)) break;
      SPID a = _maxdist(l);
      SPID b = _maxdist(r);
      if (a > b)
      {
        printlsubtree(s, r, optcost, l);
        n = l;
      }
      else
      {
        printlsubtree(s, l, optcost, r);
        n = r;
      }
      cout << ",";
    }
    printlsubtree(s, n, optcost);
    s << ")" << endl;
  }

  friend ostream& operator<<(ostream&s, UnrootedTree &p)  
  { return p.print(s); }


  void copygtcmaps()
  {
    if (nn<1) return;
    if (!map) map = new SPID[nn];
    NO map[i] = gtcclusters[i]->lcamap;
    maptau = taugtccluster->lcamap;
  }

  SPID findoptimal(RootedTree &s) {
#define shw(k)    
    if (nn<2) return 0;
    SPID cur = start;
    shw("start");
    // cur->mark(4 | 1);

    // cur should be internal,
    // note that the trees are not trivial and have at least one internal node
    if (isleaf(cur)) cur = par[cur];

    shw("init");

    int found = 0;
    if (maptau < s.lf) return cur; // |L(G)|=1, maptau is a leaf in s, DUP=|InternalNodes(G)|, LOSS=0

    off(cur);

    if (map[cur] != maptau) found = 1;
    else if (map[l3(cur)] != maptau) { found = 1; cur = l3(cur); }
    else if (map[ll3(cur)] != maptau) { found = 1; cur = ll3(cur); }

    //cout << "found=" << found << " cur=" << cur << " maptau=" << maptau << endl;
    //cur->mark();

    shw("ins");
    if (found) {
      SPID curpar = par[cur];
      while (!(isleaf(curpar))) {
        shw("wh");
        //cout << cur << " " << curpar << endl;
        off(curpar);
        if (map[l3(curpar)] != maptau) cur = l3(curpar);
        else if (map[ll3(curpar)] != maptau) cur = ll3(curpar);
        else {
          cur = curpar;
          break;
        }
        curpar = par[cur];
        //cur->mark();
      }
      if (map[cur] != maptau) return cur;
    }
    //cur->mark();
    {
      off(cur);
      if (map[par[cur]] == maptau) return cur;
      if (map[par[l3(cur)]] == maptau) return l3(cur);
      if (map[par[ll3(cur)]] == maptau) return ll3(cur);
    }
    start=cur;
    return cur;
  } // findoptimal

  int compressduplications()
  {
     int dead[nn];
     NO dead[i]=0;
     int cnt=0;
     int change=1;
     while (change)
     { 
      change=0;
      
      INTERNALNODESLOOP 
      {           
          if (!dead[i])
          {
            off(i);
            SPID l=lc3(i);
            SPID r=rc3(i);
            if (isleaf(l) && isleaf(r))
              if (lab[l]==lab[r])
              // compress
              {  
                 if ((lf-cnt)==3) break;  // trees of size max 3 are allowed
                 SPID pari=par[i];             
                 par[pari]=l;
                 par[l]=pari;
                 dead[i]=1;
                 dead[r]=1;             
                 dead[l3(i)]=1;
                 dead[ll3(i)]=1;
                 if (dead[start]) start=l;             
                 cnt++;
                 change=1;             

              }
          }
      }  
     } //while
  
  return cnt;
}

  int cherrynumber()
  {
    
    if (lf==1) return 0;
    if (lf==2) return 1;    
    if (lf==3) return 3;
    int chn=0;
    INTERNALNODESLOOP
    {
      off(i);
      SPID l=lc3(i);
      SPID r=rc3(i);
      if (isleaf(l) && isleaf(r)) chn++;
    }
    return chn;
  }


  // unrooted tree
  void preparemaplist(SPID *gdup, SPID *gspec, SPID *duplen, SPID *speclen, int create = 1)
  {
    if (nn<1) return;
    // speclen, duplen must be zero
    //cout << "!" << nn << "." << lf << endl;
    if (create) 
    {
      mdup = new SPID[nn]; // s dup mappings list (-1 end)
      mspec = new SPID[nn]; // s spec mappings list (-1 - end)
    }
    for (int i = 0; i < nn; i++) {
      mdup[i] = -1;
      mspec[i] = -1;
      SPID m = map[i];
      if (isinternalnode(i))
      {
        off(i);
        SPID l = lc3(i);
        SPID r = rc3(i);
        if (map[l] == m || map[r] == m)
        {
          // duplication
          mdup[i] = gdup[m];
          gdup[m] = i;
          duplen[m]++;
          continue;
        }
      }
      // speciation or leaf
      speclen[m]++;
      mspec[i] = gspec[m];
      gspec[m] = i;
    }
  }
}; // UnrootedTree


class TreeRepr
{
public:
  int len;
  SPID *t;
  COSTTYPE cost;
  char status;
  int generation;
  int lastedit;
  TreeRepr(SPID *_t, int _len, COSTTYPE _cost, int _generation, int _lastedit) : len(_len), t(_t), cost(_cost), generation(_generation), lastedit(_lastedit)
  { status = TRNEW; }
  int eq(SPID *u, int l, COSTTYPE c)
  {
    if (cost != c) return 0;
    if (len != l) return 0;
    for (int i = 0; i < len; i++) if (t[i] != u[i]) return 0;
    return 1;
  }

  int eq2(SPID *u, int l)
  {
    if (len != l) return 0;
    for (int i = 0; i < len; i++) if (t[i] != u[i]) return 0;
    return 1;
  }

  friend ostream& operator<<(ostream&s, const TreeRepr &t)
  {
    for (int i = 0; i < t.len; i++)
    {
      if (t.t[i] == MAXSP) s << "(";
      else if (abs(t.t[i]) == (1 + MAXSP)) s << ")";
      else s << species(abs(t.t[i]));
      if (t.t[i] <= 0) s << ",";
    }
    return s;
  }
};

void RootedTree::replace(TreeRepr *tr)
{
  string trs = to_string(*tr);
  //verb_det << endl << "Replacing " << trs << endl;

  SPID fl = 0, fi = lf;
  int p = 0;
  char *c = strdup(trs.c_str());
  root = parse(c, p, 0, fl, fi);
  par[root] = MAXSP;
  free(c);
  exactspecies = 0;

}

typedef std::vector<TreeRepr*> vtr;
typedef std::map<COSTTYPE, vtr > maptype; // @todo: better repr

class treereprcomp
{ public:  bool operator() (const TreeRepr *a, const TreeRepr *b) const { return a->cost > b->cost; } };

typedef priority_queue< TreeRepr*, vector<TreeRepr*>, treereprcomp > ptr;

class ArrTab
{
public:
  maptype m;
  int size;
  int optnum;
  COSTTYPE opt;
  ptr waiting;

  ArrTab() { size = 0; optnum = 0; }

  void add2waiting(TreeRepr *tr)  { waiting.push(tr); }

  TreeRepr *getwaiting(int del = 1)
  {
    if (!waiting.size()) return NULL;
    TreeRepr *r = waiting.top();
    if (del) waiting.pop();
    return r;
  }

  

  void printwaiting(int num = 6)
  {
    printf("\nWaiting queue\n");
    ptr waiting2;
    while (waiting.size())
    {
      TreeRepr *r = waiting.top();
      waiting.pop();
      waiting2.push(r);
      if (num > 0)
        cout << r->cost << " " << *r << endl;
      num--;
    }
    cout << endl;
    waiting = waiting2;
    printf("\n");
  }

#define TAB_NEWOPTIMAL 3
#define TAB_NEXTOPTIMAL 2
#define TAB_NEWNONOPTIMAL 1
#define TAB_TREEPRESENT 0
#define TAB_COSTERROR 4


  void insertinto(ArrTab *at)
  {
    int cnt=0;
    for ( maptype::iterator ii = m.begin(); ii != m.end(); ++ii)
    {
      vtr &v = (*ii).second;      
      for ( vtr::iterator i = v.begin(); i != v.end(); ++i)
      {
        TreeRepr *t, *tr=(*i);
        at->add(tr->t, tr->len, tr->cost, t, tr->generation, tr->lastedit);
        cnt++;
      }
      if (cnt>=TAB_MAXSIZE) return;
    }
  }

  // Return 3 if new opt is found
  // Return 2 if another opt is found
  // Return 1 if new non-optimal tree
  // Return 0 tree is present
  // Return 4 if tree is present with different cost (DEBUG only)
  int add(SPID *t, int len, COSTTYPE cost, TreeRepr *&tr, int generation, int lastedit)
  {
    tr = NULL;
#ifdef NNIDEBUGTREEREPR
    // additional control of costs
    for ( maptype::iterator ii = m.begin(); ii != m.end(); ++ii)
    {
      vtr &v = (*ii).second;
      for (vtr::iterator i = v.begin(); i != v.end(); ++i)
        if ((*i)->eq2(t, len) && (cost != (*i)->cost))
        {
          tr = new TreeRepr(t, len, cost,generation,lastedit);
          cout << "Cost problem?" << endl;
          cout << "new" << *tr << " cost=" << cost << endl;
          cout << "old" << *(*i) << " cost=" << (*i)->cost << endl;
          return TAB_COSTERROR;
        }
    }
#endif

    maptype::iterator it = m.find(cost);
    if (it == m.end())
    {
      if (!m.size()) opt = cost + 1;
      m[cost] = vtr();
    }
    else
    {
      for ( vtr::iterator ii = m[cost].begin(); ii != m[cost].end(); ++ii)
        if ((*ii)->eq(t, len, cost))
        {
          tr = *ii;
          return TAB_TREEPRESENT;
        }
    }

    SPID *tn = new SPID[len];
    for (int i = 0; i < len; i++) tn[i] = t[i];
    m[cost].push_back(tr = new TreeRepr(tn, len, cost, generation, lastedit));
    size++;
    if (cost < opt) {
      opt = cost;
      optnum = 1;
      return TAB_NEWOPTIMAL;
    }
    if (cost == opt) { optnum++; return TAB_NEXTOPTIMAL; }
    return 1;
  }

  COSTTYPE optimal() { return opt; }


  TreeRepr *getoptimaltree()
  {
    for ( maptype::iterator ii = m.begin(); ii != m.end(); ++ii)
    {
      vtr &v = (*ii).second;      
      for ( vtr::iterator i = v.begin(); i != v.end(); ++i)      
        return (*i);              
    }
    return 0;    
  }  

  int exists(SPID *t, int len, COSTTYPE cost)
  { return 0; }

  friend ostream& operator<<(ostream&s, ArrTab &a)
  {
    int cnt=0;
    for ( maptype::iterator ii = a.m.begin(); ii != a.m.end(); ++ii)
    {
      vtr &v = (*ii).second;      
      for ( vtr::iterator i = v.begin(); i != v.end(); ++i)
      {
        s << (*ii).first << " " << *(*i) << endl;
        cnt++;
        if (cnt==TAB_MAXSIZE) return s;

        //TODO: via variable
        if (cnt==inferredspeciesrteescount) return s;
      }
    }
    return s;
  }

  string filetosave()
  {    

    time_t rawtime;
    struct tm * timeinfo;
    char buffer [80];

    if (savetabstyle == ARR_SIMPLE)
    {
      sprintf(buffer, "fu.txt");
    }
    else if (savetabstyle == ARR_COST)
    {
      sprintf(buffer, "fu.%s.%g.%d.txt", projectname, optimal(), getpid());
    }
    
    else {
      time ( &rawtime );
      timeinfo = localtime ( &rawtime );
      strftime (buffer, 80, "%Y%m%d.%H%M%S.fu.txt", timeinfo);
    }
    return string(buffer);

  }

  void save(int append=0, const char *filename=NULL)
  {
    ofstream s;
    string f;
    if (!filename)  f=filetosave();
    else f=string(filename);

    // cout << "save:" << append << " " << f << endl;

    if (append) s.open(f.c_str(),fstream::out | fstream::app);
    else s.open (f.c_str());
    s << *this;
    s.close();
    if (!append)
      verb_smp << f << " saved" << endl;
  }

};


#define BUFSIZE 100000

void readtrees(char *fn, vector<char*> &stvec)
{
  FILE *f;
  f = fopen(fn, "r");
  if (!f)
  {
    cerr << "Cannot open file " << fn << endl;
    exit(-1);
  }
  char buf[BUFSIZE];
  while (1)
  {
    if (!fgets(buf, BUFSIZE, f)) break;
    stvec.push_back(strdup(buf));
  }
  fclose(f);
}

void readtreesinterleaved(char *fn, vector<char*> &gtvec, vector<char*> &stvec)
{
  FILE *f;
  f = fopen(fn, "r");
  if (!f)
  {
    cerr << "Cannot open file " << fn << endl;
    exit(-1);
  }
  int num = 0;
  char buf[BUFSIZE];
  while (1)
  {
    if (!fgets(buf, BUFSIZE, f)) break;
    if (num % 2)
      stvec.push_back(strdup(buf));
    else
      gtvec.push_back(strdup(buf));
    num++;
  }
  fclose(f);
}




SPID RootedTree::_outdistribution(SPID node, SPID *duplen, SPID *speclen, SPID *mout)
{
  if (isleaf(node))
    return mout[node] = speclen[node] - duplen[node];
  return mout[node] = max(0, -speclen[node] - duplen[node]
                          + _outdistribution(leftchild(node), duplen, speclen, mout)
                          + _outdistribution(rightchild(node), duplen, speclen, mout));
}

SPID RootedTree::_checkoutdistribution(SPID node, SPID *duplen, SPID *speclen, SPID *mout, int &ok)
{
  if (isleaf(node))
  {
    ok = mout[node] == speclen[node] - duplen[node];
    mout[node] = speclen[node] - duplen[node];
    if (!ok)
      cout << "Leaf mout problem";
    return mout[node];
  }
  SPID a = _checkoutdistribution(leftchild(node), duplen, speclen, mout, ok);
  if (!ok) return 0;
  SPID b = _checkoutdistribution(rightchild(node), duplen, speclen, mout, ok);
  if (!ok) return 0;
  ok = mout[node] == max(0, -speclen[node] - duplen[node] + a + b);
  if (!ok)
  {
    cout << "node=" << node << " mout=" << mout[node] << " expected=" << (-speclen[node] - duplen[node] + a + b) << endl;
  }
  mout[node] = max(0, -speclen[node] - duplen[node] + a + b);
  return -speclen[node] - duplen[node] + a + b;
}

// DlLoss Cost
SPID RootedTree::_outdlcost(SPID node, SPID *duplen, SPID *speclen, DlCost &c)
{
  c.dup += duplen[node];
  if (isleaf(node)) return speclen[node] - duplen[node];
  SPID o1 = _outdlcost(leftchild(node), duplen, speclen, c);
  SPID o2 = _outdlcost(rightchild(node), duplen, speclen, c);
  c.loss += o1 + o2 - 2 * speclen[node];
  return o1 + o2 - speclen[node] - duplen[node];
}

void RootedTree::outdlcost(SPID maptau, SPID *duplen, SPID *speclen, DlCost &c, SPID LGsize)
{  
  c.dup = 0;
  c.loss = 0;
  if (isleaf(maptau))
  {
    c.dup = LGsize - 1;
    return;
  }
  SPID o1 = _outdlcost(leftchild(maptau), duplen, speclen, c);
  SPID o2 = _outdlcost(rightchild(maptau), duplen, speclen, c);
#ifdef NNIDEBUG
  cout << "Bef root" << c << endl;
#endif
  if (duplen[maptau] + speclen[maptau] == 2 * LGsize - 4) return;
#ifdef NNIDEBUG
  cout << o1 << " " << o2 << " " << speclen[maptau] << endl;
#endif
  c.loss += o1 + o2 - 2 * speclen[maptau];
  c.dup += o1 + o2 - speclen[maptau] - 1;
}

void RootedTree::nniinit(GTS &g, int create)
{
  // gmap pointers and losses
  if (create)
  {
    mdup = new SPID*[g.size()];
    mspec = new SPID*[g.size()];
    mout = new SPID*[g.size()];
  }

  for (int i = 0; i < (int)g.size(); i++)
  {
    if (create)
    {
      mdup[i] = new SPID[nn];
      mspec[i] = new SPID[nn];
      mout[i] = new SPID[nn];
    }
    for (int k = 0; k < nn; k++) {
      mdup[i][k] = -1;
      mspec[i][k] = -1;
      mout[i][k] = 0;
    }
  }

  // prepare g-double lists and initialize maps
  int maxgt = 0;
  SPID tdup[nn];
  SPID tspec[nn];
  for (int i = 0; i < (int)g.size(); i++)
  {
    // reset len tables
    for (int j = 0; j < nn; j++) tdup[j] = tspec[j] = 0;
    // create map list and lengths
    g[i]->preparemaplist(mdup[i], mspec[i], tdup, tspec, create);
    // gen mout
    _outdistribution(root, tdup, tspec, mout[i]);
    if (g[i]->nn > maxgt) maxgt = g[i]->nn;
  }
  // prepare array for storing maps-update info
  mapupdate = new char[maxgt];
}

// if start from a new species tree
void RootedTree::nnireinit(GTS &g)
{
  initlca();
  for (int i = 0; i < (int)g.size(); i++)
  {
    g[i]->clean();
    g[i]->setlcamappings(*this);
    for (int k = 0; k < nn; k++) {
      mdup[i][k] = -1;
      mspec[i][k] = -1;
      mout[i][k] = 0;
    }
  }

  // prepare g-double lists and initialize maps
  int maxgt = 0;
  SPID tdup[nn];
  SPID tspec[nn];
  for (int i = 0; i < (int)g.size(); i++)
  {
    // reset len tables
    for (int j = 0; j < nn; j++) tdup[j] = tspec[j] = 0;
    // create map list and lengths
    g[i]->preparemaplist(mdup[i], mspec[i], tdup, tspec, 0);
    // gen mout
    _outdistribution(root, tdup, tspec, mout[i]);
    if (g[i]->nn > maxgt) maxgt = g[i]->nn;
  }
  // prepare array for storing maps-update info
  mapupdate = new char[maxgt];
}

SPID RootedTree::_nniupdate(UnrootedTree *g, SPID gn, SPID t, SPID x)
{
  if (mapupdate[gn]) return (mapupdate[gn] & MUT) ? t : x;
  // gn cannot be a leaf (t - is not a leaf)
  SPID l, r;
  g->getchildren(gn, l, r);
  SPID lmap = g->map[l];
  SPID rmap = g->map[r];

  if (lmap == t) lmap = _nniupdate(g, l, t, x);
  else if (lmap == x) lmap = t;
  if (rmap == t) rmap = _nniupdate(g, r, t, x);
  else if (rmap == x) rmap = t;

  if ((lmap == t) || (rmap == t))
  {
    mapupdate[gn] = MUTDUP;
    return t;
  }

  // rest is c+a/x,a,b,c

  // (b,c+a/x)
  // t is speciation
  if (isparent(x, rmap, t) && !isparent(x, lmap, t))
  {
    mapupdate[gn] = MUTSPEC;
    return t;
  }

  // (c+a/x, b)
  // t is speciation
  if (isparent(x, lmap, t) && !isparent(x, rmap, t))
  {
    mapupdate[gn] = MUTSPEC;
    return t;
  }

  // rest is mapped into x, c, a
  // duplication or speciation?
  if ((lmap == x)   || (rmap == x))
  {
    mapupdate[gn] = MUXDUP;
    return x;
  }

  // must be x-speciation
  mapupdate[gn] = MUXSPEC;
  return x;
}

class CC {
public:
  cluster c;
  string s;
  CC(cluster _c, string _s) : c(_c), s(_s) {}
};






void RootedTree::_nniprint(string s, UnrootedTree &g, int gtnum, ostream &c)
{
  if (s.size())
    c << "SPECIES TREE " << s << endl;
  for (int j = 0; j < nn; j++)
  {
    printdebnode(c, j);
    SPID gn = mspec[gtnum][j];
    c << "  out=" << mout[gtnum][j] << " ";
    c << "  gspec=[";
    while (gn >= 0)
    {
      c << " " << (SPC)gn;
      gn = g.mspec[gn];
    }
    c << "] ";

    gn = mdup[gtnum][j];
    c << "  gdup=[";
    while (gn >= 0)
    {
      c << " " << (SPC)gn;
      gn = g.mdup[gn];
    }
    c << "]" << endl;
  }
}

// initialize computations for nni's
TotalCost RootedTree::gtccost(GTS &gt)
{
  //cout << "!";
  setdepth();
  initlca();
  GTS::iterator gtpos;
  TotalCost total(0,0);

  gtc->setlcamappings(*this);

  for (gtpos = gt.begin(); gtpos != gt.end(); ++gtpos) {
    UnrootedTree *g = *gtpos;
    g->copygtcmaps();
    SPID n = g->findoptimal(*this);
    DlCost c = g->dlcostsmp(*this, n);    
    total.update(c);
    c=DlCost(g->fixedduplications,0);
    total.update(c);
  }

  //cout << "TOTAL" << total << initGTS(gt,1,1) << endl;
  return total;
}

// initialize computations for nni's
TotalCost RootedTree::initGTS(GTS &gt, int first, int initnnistructs)
{

  //cout << "initgts" << first << " " << initnnistructs << endl;
  setdepth();
  GTS::iterator gtpos;
  TotalCost total(0,0);

  // prepare gtrees
  for (gtpos = gt.begin(); gtpos != gt.end(); ++gtpos) {
    UnrootedTree *g = *gtpos;
    g->clean();
    g->setlcamappings(*this);
  }

  // initialize nni structures
  if (initnnistructs)
  {
    if (first) nniinit(gt);
    else nnireinit(gt);
  }

  for (gtpos = gt.begin(); gtpos != gt.end(); ++gtpos) {
    UnrootedTree *g = *gtpos;
    SPID n = g->findoptimal(*this);
    DlCost c = g->dlcost(*this, n);
    total.update(c);
    g->nnicost = c;
    
    c=DlCost(g->fixedduplications,0);
    total.update(c);
    
  }
  
  pcct(-2,total);
  return total;
}




int RootedTree::nnirot(SPID r, GTS &gt, TotalCost &costdelta, int &xcase, string &info, SPID artroot)
{
    SPID p=par[r];
    SPID sibr=sibling(r);
    SPID parp=-1;
    int leftside=leftchild(p)==r;

    if (leftside && isleaf(sibr))
    {      
        // swap only
#ifdef SPRNDEBUG      
        cout << "1.SPRn swapch " << p << endl;
#endif        
        swapchildren(p);
        return 10+nnirot(r,gt,costdelta,xcase,info,artroot);
        // continue with the next move
    }

    if (leftside) //&& !isleaf(leftchild(sibr)))
    {      
#ifdef SPRNDEBUG            
        cout << "2.SPRn swapch+nni " << p << " " << p << endl;
#endif        
        swapchildren(p);        
        nni(p, 1, gt, costdelta, xcase, info);        
        return 2;
        
    }

    if (artroot!=p) parp=par[p];

    if (!leftside && artroot!=p) 
    {
        
        if (p==rightchild(parp))
        {
#ifdef SPRNDEBUG                      
            cout << "3.SPRn nni+swap " << parp << " " << p << endl;
#endif            
            int res=nni(parp, 0, gt, costdelta, xcase, info);
            if (res) swapchildren(parp);        
            return 3;
        }
#ifdef SPRNDEBUG                          
       cout << "4.SPRn swap+nni+swap " << parp << " " << p << endl;
#endif      
       swapchildren(p);

       int res=nni(parp, 1, gt, costdelta, xcase, info);
       if (res) { 
        swapchildren(leftchild(parp));
        swapchildren(parp);
       } 
       return 4;         
        
    }

    if (p==artroot && !leftside)
    {
        if (!isleaf(sibr))
        {
#ifdef SPRNDEBUG                                 
            cout << "5.SPRn nni" << p << " " << endl;
#endif                
            nni(p, 1, gt, costdelta, xcase, info);
            return 5;
        }
    }
  
    return 0;
}

// Each gene tree should store previous cost in nnicost attribute (updated here)
int RootedTree::nni(SPID t, int left, GTS &gt, TotalCost &costdelta, int &xcase, string &info)
{

#define trav(nd,tb) gn=im##tb[nd]; while (gn>=0)  { SPID next=g->m##tb[gn];
#define endtrav() gn=next;  }

#define travsmp(nd,tb,op) gn=im##tb[nd]; while (gn>=0) { op; gn=g->m##tb[gn]; }
#define travnsmp(nd,tb,op) gn=im##tb[nd]; while (gn>=0) { SPID next=g->m##tb[gn]; op; gn=next; }
  SPID x;

  if (left) x = leftchild(t);
  else x = rightchild(t);

  if (isleaf(x)) return 0;

#ifdef NNIDUMP
  dumpnnistructs(gt);
#endif

  costdelta.clear();

#ifdef NNIDEBUG  
  _nniprint("before", *gt[0]);
#endif

  SPID a = leftchild(x);
  SPID b = rightchild(x);
  SPID c;

  if (left) c = rightchild(t);
  else c = leftchild(t);

#ifdef NNIDEBUG
  cout << " @T=" << (SPC)t << " ";      printsubtree(cout, t) << endl;
  cout << " @X=" << (SPC)x << " ";      printsubtree(cout, x) << endl;
  cout << " @A=" << (SPC)a << " ";      printsubtree(cout, a) << endl;
  cout << " @B=" << (SPC)b << " ";      printsubtree(cout, b) << endl;
  cout << " @C=" << (SPC)c << " ";      printsubtree(cout, c) << endl;
#endif

  // NNI move
  par[b] = t;
  par[c] = x;
  leftchild(x) = c;
  rightchild(x) = a;
  
  if (left) rightchild(t) = b;
  else leftchild(t) = b;

  GTS::iterator gtpos;

  

  for (int i = 0; i < (int)gt.size(); i++)
  {

    UnrootedTree *g = gt[i];
    int type = 1; // STN, -1 - root, 0 - no change
    SPID *imdup = mdup[i];
    SPID *imspec = mspec[i];

    DlCost curdelta(0, 0);
    
#ifdef NNIDEBUG
    for (int k = 0; k < g->nn; k++) mapupdate[k] = 0;
#endif


    // find the top mapping for G, starting from x
    SPID cur = x;
    SPID last = -1;
    while (1)
    {
      if (imdup[cur] != -1 || imspec[cur] != -1) last = cur;
      if (cur == root) break;
      cur = par[cur];
    }

    if (last == -1)
    {
#ifdef NNIDEBUG
      cout << last << " LG NON x,t,...,root mappings - no change in cost " << endl;
#endif
      type = 0;
      continue;
    }
    else if (last == x || last == t)
    {
#ifdef NNIDEBUG
      cout << last << " LG ROOT case - recomputing " << x << " " << t << endl;
#endif
      type = -1;
    }
#ifdef NNIDEBUG
    else cout << last << " LG STN case " << endl;
    cout << "t=" << (SPC)t << " x=" << (SPC)x << " to be updated" << endl;
#endif

    SPID gn;
    // 1. Traverse dbl-lists, initial mappings

    // change in dup and specs
    int dpX = 0, spX = 0, dpT = 0, spT = 0;
    int prevXdup = 0, prevTdup = 0, aL = 0;

#define addXTloss() SPID mappar=g->dirmapparent(gn); \
      ncout(gn << " MPAR=" << mappar << " x=" << x << " t=" << t <<endl); \
      if ((mappar!=x) && (mappar!=t)) { aL++; ncout(gn << " " << g->par[gn] << " " << mappar << " tut aL"); }

    travsmp(x, spec, mapupdate[gn] = MUTSPEC);
    travsmp(x, dup, mapupdate[gn] = MUTDUP; prevXdup++);
    travsmp(t, spec, mapupdate[gn] = 0);
    travsmp(t, dup, mapupdate[gn] = 0; prevTdup++);

    // 2. Recursively update unknown mappings
    travsmp(t, spec, if (!mapupdate[gn]) _nniupdate(g, gn, t, x));
    travsmp(t, dup, if (!mapupdate[gn]) _nniupdate(g, gn, t, x));

  
#ifdef NNIDEBUG
    cout << "GENE TREE before (mapupdate created)" << endl;
    for (int j = 0; j < g->nn; j++)
    {
      g->printdebnode(cout, j);
      cout << "  mdup=" << setw(3) << (SPC)g->mdup[j];
      cout << "  mspec=" << setw(3) << (SPC)g->mspec[j];
      if (mapupdate[j])
      {
        cout << "  mu=";
        if (mapupdate[j] & MUT) cout << "T";
        if (mapupdate[j] & MUX) cout << "X";
        if (mapupdate[j] & MUSPEC) cout << "~";
        if (mapupdate[j] & MUDUP) cout << "+";
      }
      cout << endl;
    }
#endif

#ifdef NNIDEBUG
#define ncout(x) cout << x << endl
#else
#define ncout(x)
#endif


    // 3. Cost update
    // 3.1. x - nodes
    int lsC2 = 0, lsX2 = 0, lsB = 0, lsA = 0, lsC = 0, lsX = 0, lsA2 = 0, lsB2 = 0;
    SPID l, r;

    //==============
    // 1.A (a,b)
    travsmp(x, spec, lsC2++; spX--; spT++; ncout("+1L (a,b) 1x"); ); // replace with +len

    //==============
    trav(x, dup);
    dpX--; dpT++;
    g->getchildren(gn, l, r);
    SPID lm = g->map[l];
    SPID rm = g->map[r];

    if (lm == x && rm != x)
    {
      if (isparent(a, rm, t)) { lsX2++; lsC2++; lsB--; ncout("+1L (a+b,a) 4x");  }
      else { lsA--; lsX2++; ncout("0 var(a+b,b) 3x"); }
    }

    if (rm == x && lm != x)
    {
      if (isparent(a, lm, t)) { lsX2++; lsC2++; lsB--; ncout( "+1L (a,a+b) 4x"); }
      else { lsA--; lsX2++; ncout("0 var(b,a+b) 3x"); }
    }
    endtrav(); // x dup

    //==============
    trav(t, spec);

    //#define trav(nd,tb) 
    // gn=imspec[t]; 
    // while (gn>=0)  { 

    //   SPID next=g->mspec[gn];
    
    if (mapupdate[gn] == MUTSPEC) { lsA--; lsA2++; ncout("0 (b,c) 2t"); }
    else if (mapupdate[gn] == MUTDUP) { /* 3t */ lsB2++; lsA2++; spT--; dpT++; ncout("+2L (a+b,c) 3t"); }
    else if (mapupdate[gn] == MUXSPEC) {
      /* 1t */ 
      lsB--; spT--; spX++; 
      ncout("-1L (a,c) 1t"); 
      
      addXTloss();  
      
      // SPID mappar=g->dirmapparent(gn); 
      // ncout(gn << " MPAR " << mappar << endl); 
      // if ((mappar!=x) && (mappar!=t)) 
      //   { aL++; ncout(gn << " " << g->par[gn] << " " << mappar << " tut aL"); }

    }
    endtrav(); // t spec
    //gn=next;  }

    //==============
    // t-SPECIATION, DUPLICATION
    trav(t, dup);
    g->getchildren(gn, l, r);
    SPID lm = g->map[l];
    SPID rm = g->map[r];

    if (mapupdate[gn] == MUTSPEC) { /* 6t */ lsA--; lsC--; dpT--; spT++; ncout("0 var(a+c,b) 6t"); } //MUTSPEC
    else if (mapupdate[gn] == MUXDUP) {
      // 4t, 8t, 11t
      dpT--; dpX++;
      if (lm == t)
      {
        if (rm != t)
        {
          if (isparent(c, rm, t)) { lsX--; lsA2++; ncout("0 (a+c,c) 8t"); }
          else { lsC--; lsB--; lsC2++; ncout("-L (a+c,a) 4t"); }
        }
      } else if (rm == t)
      {
        if (isparent(c, lm, t)) { lsX--; lsA2++; ncout("0 (c,a+c) 8t"); }
        else { lsC--; lsB--; lsC2++; ncout( "-L (a,a+c) 4t"); }
      }
      
      addXTloss();
      
    } // MUXDUP
    else if (mapupdate[gn] == MUTDUP)
    {
      if (isparent(a, lm, t) || isparent(a, rm, t)) {/*5t*/ lsB--; lsC--; lsC2++; lsB2++; ncout( "0 (a,b+c/t) 5t"); }
      else if (isparent(b, lm, t) || isparent(b, rm, t)) { /*7t*/ lsA--; lsC--; lsX2++; ncout( "-L (b,b+c/t) 7t"); }
      else if (isparent(c, lm, t) || isparent(c, rm, t)) { /*9t*/ lsX--; lsB2++; lsA2++; ncout( "-L (c,b+c/t) 9t"); }
      /* 10t 12t 13t 14t */
      else if (isparent(x, lm, t) || isparent(x, rm, t))
      {
        // 10t,12t
        if (mapupdate[l]&MUT && mapupdate[r]&MUT) { /*12t*/ lsC--; ncout( "-L (b+c/t,a+b) 12t"); }
        else { lsC--; lsB2++; ncout( "0 (a+c,a+b) 12t"); }
      }
      // 13t 14t
      else if (mapupdate[l]&MUX || mapupdate[r]&MUX) { /*13t*/ lsB2++; ncout( "+L (b+c/t,a+c) 13t"); }
    }
    endtrav();

    // 4. Update lists and maps
    // 4.1 extract t->x nodes into new list
    // traverse t lists and update maps
    // 4.3 set new list to x gmapm
    
    curdelta.loss = lsA + lsB + lsC + lsX + lsA2 + lsB2 + lsC2 + lsX2;
    curdelta.dup = dpT + dpX;

    //pcc(19,curdelta);        

    // additional loss update
    ncout( "nnilosses: " << costdelta.loss << "L" << endl);
    SPID *out = mout[i];

#ifdef NNIDEBUG
    SPID rA = (out[x] + prevXdup - out[a]) + lsA;
    SPID rB = (out[x] + prevXdup - out[b]) + lsB;
    SPID rC = (out[t] + prevTdup - out[c]) + lsC;
    SPID rX = (out[t] + prevTdup - out[x]) + lsX;
    SPID uC = rC - rA - rB;
    cout << " rA=" << rA;
    cout << " rB=" << rB;
    cout << " rX=" << rX;
    cout << " rC=" << rC;
    cout << " aL=" << aL; ///!
    cout << " uC=" << uC;
    cout << endl;
    cout << "addlosses rX-rA-uC: " << rX - rA - uC + aL << "L" << endl;
#endif
    curdelta.loss += lsX + out[c] - lsC + prevXdup - out[b] + lsB + aL;

#define addlist(what,typ,head) if (mapupdate[gn]==what) { g->m##typ[gn]=head; head=gn;  }

    SPID tdup = imdup[x];  // x-dup -> new t-dup list
    SPID tspec = imspec[x]; // x-spec -> new t-spec list

    // new x lists
    SPID xdup = -1;
    SPID xspec = -1;

    travsmp(x, spec, g->map[gn] = t);
    travsmp(x, dup, g->map[gn] = t);

    trav(t, spec);
    if (mapupdate[gn]&MUT) g->map[gn] = t;
    if (mapupdate[gn]&MUX) g->map[gn] = x;
    addlist(MUTDUP, dup, tdup);
    addlist(MUTSPEC, spec, tspec);
    addlist(MUXSPEC, spec, xspec);
    addlist(MUXDUP, dup, xdup);
    endtrav();

    trav(t, dup);
    if (mapupdate[gn]&MUT) g->map[gn] = t;
    else if (mapupdate[gn]&MUX) g->map[gn] = x;
    addlist(MUTDUP, dup, tdup);
    addlist(MUTSPEC, spec, tspec);
    addlist(MUXDUP, dup, xdup);
    addlist(MUXSPEC, spec, xspec);
    endtrav();

    imdup[t] = tdup;
    imspec[t] = tspec;

    imdup[x] = xdup;
    imspec[x] = xspec;

    // update mout of x node
    int splenx = 0;
    int dplenx = 0;
    travsmp(x, spec, splenx++);
    travsmp(x, dup, dplenx++);
    out[x] = -splenx - dplenx + out[a] + out[c];
    //if (i==4)
    //  cout << "XINFO: " << splenx << " " << dplenx << " " << out[a] << " " << out[c] << endl;

    

    if (type == -1)
    {
      if (imdup[t] == -1 && imspec[t] == -1) g->maptau = x;
      else g->maptau = t;

      // checking tau cost
      SPID dplen[nn];
      SPID splen[nn];
      for (int k = 0; k < nn; k++) {
        dplen[k] = 0;
        splen[k] = 0;
        travsmp(k, spec, splen[k]++);
        travsmp(k, dup, dplen[k]++);
      }
      DlCost tt;
      //  ncout( "maptau";
      _outdistribution(root, dplen, splen, mout[i]);
      outdlcost(g->maptau, dplen, splen, tt, g->lf);
      //ncout( "TestCost " << tt << " " << (g->nnicost + costdelta));      
      curdelta.loss = tt.loss - g->nnicost.loss;
      curdelta.dup = tt.dup - g->nnicost.dup;
      g->nnicost = tt;
    }
    else
      g->nnicost = g->nnicost + curdelta;

    costdelta.update(curdelta);
    //costdelta.dup += curdelta.dup;


#ifdef NNIVERIFY
    SPID dplen[nn];
    SPID splen[nn];
    for (int k = 0; k < nn; k++) {
      dplen[k] = 0;
      splen[k] = 0;
      travsmp(k, spec, splen[k]++);
      travsmp(k, dup, dplen[k]++);
    }
    DlCost tt;
    outdlcost(g->maptau, dplen, splen, tt, g->lf);
    int ok = 1;

    if (tt.loss != g->nnicost.loss)
    {
      cout << i << " " << type << " TT Problem (outs?) " << tt << " " << g->nnicost << endl;
      dumpnnistructs(gt, i, 8888);
      moutproblem = 1;
    }
    _checkoutdistribution(root, dplen, splen, mout[i], ok);
    if (!ok)
    {
      cout << i << " " << "OUT DISTIB. PROBLEM ";
      //dumpnnistructs(gt,i,9999);
      //exit(-1);
    }
#endif


#ifdef NNIDEBUG
    cout << "GENE TREE after" << endl;

    for (int j = 0; j < g->nn; j++)
    {
      g->printdebnode(cout, j);
      cout <<  "  mdup=" << setw(3) << (SPC)g->mdup[j];
      cout <<  "  mspec=" << setw(3) << (SPC)g->mspec[j];
      if (mapupdate[j])
      {
        cout << "  mu=";
        if (mapupdate[j] & MUT) cout << "T";
        if (mapupdate[j] & MUX) cout << "X";
        if (mapupdate[j] & MUSPEC) cout << "~";
        if (mapupdate[j] & MUDUP) cout << "+";
      }
      cout << endl;
    }
    cout << endl;
#endif

  } // gene tree loop

#ifdef NNIDEBUG
  _nniprint("after", *gt[0]);
#endif

  return 1;
}

string quoted(char *s)
{
  return string("\"") + s + string("\"");
}



RootedTree* TreeClusters::genrooted(RootedTree *preserveroottree)
{
  
  vector<GTCluster*> sc,compclusters;
  for (int i=0;i<internal.size();i++) sc.push_back(internal[i]);
  for (int i=0;i<leaves.size();i++) sc.push_back(leaves[i]);
  sort(sc.begin(),sc.end(),compcnt);

  // for (int i=0;i<specnames.size();i++) cout<< i << "-" << specnames[i] << " ";
  //   cout << endl;

  SPID lc,rc;
  if (preserveroottree)
  {
    SPID** t=preserveroottree->getspclusterrepr();
    preserveroottree->getchildren(preserveroottree->root,lc,rc);

    //printspcluster(cout,t[lc]);
    //cout << " \t --> \t ";
    //printspcluster(cout,t[rc]);
    //cout << endl; 
    
    compclusters.push_back(new GTCluster(0,0,t[lc]));
    compclusters.push_back(new GTCluster(0,0,t[rc]));    

    // TODO: Clean array t

  }

  int maxcnt=sc[0]->usagecnt;
  float minusage=0.01*maxcnt;

  // for (int i=0;i<sc.size();i++)
  //   { cout << "cr#" << sc[i]->usagecnt << " cluster=" << *sc[i] << " ";      
  //     if (sc[i]->spcluster[0]<specnames.size()/2 && sc[i]->spcluster[0]>1)
  //       if (sc[i]->spcluster[0]==1)
  //         cout << "+";
  //       cout << endl;

  //     }

    for (int i=0;i<sc.size();i++)
    {   
        GTCluster *gc=sc[i];
        if (gc->spcluster[0]>1 && (gc->spcluster[0]>0.7*specnames.size() || 
            gc->usagecnt<minusage)) continue;

        int ok=1;        
        
        for (int j=0;j<compclusters.size();j++)
        {

            SPID *cur=compclusters[j]->spcluster;            
            if (cur==topspcluster) continue;
                                    
            
            if (spsubseteq(gc->spcluster,cur) && spsubseteq(cur, gc->spcluster)) { ok=0; break; }

            SPID* sum=joinspclusters(cur,gc->spcluster);  // genrooted
            
            if (spsize(sum)==spsize(gc->spcluster) 
                || spsize(sum)==spsize(cur) || spsize(sum)==spsize(gc->spcluster)+spsize(cur)) { 
                deletespcluster(sum);
                continue; 
                } // inclusion/disjont

            deletespcluster(sum);

            ok=0;
            break;      
        }

        if (ok) { compclusters.push_back(gc); 
            //cout << "Inserting " << *gc << endl;
            } 
        //else cout << "Failure" << endl;
    }

  // Compatible clusters 
  sort(compclusters.begin(),compclusters.end(),compsize);


  vector<GTCC> vs;
  for (int i=0;i<compclusters.size();i++)   
  {
    GTCluster *c = compclusters[i];

    // cout << "  CUR=" << *c << endl;

    if (c->spcluster[0]==1) // leaf
    {
       vs.push_back(GTCC(c->spcluster, species(c->spcluster[1]))); 
       continue;
    }

    vector<GTCC> res; 
    for (int j = 0; j < (int)vs.size(); j++)
      if (spsubseteq(vs[j].c, c->spcluster))
      {
        res.push_back(vs[j]);
        vs.erase(vs.begin() + j);
        j--;
      }

    
    //for (int j=0;j<res.size();j++ ) { cout << "z "; printspcluster(cout,res[j].c); cout << endl; }

    if (res.size() < 2)
    {
      cerr << "At least two subsets in genrooted expected..." << endl;
      return NULL;
    }


    while (res.size()>1)
    {
        int a = rand()%res.size();
        GTCC l = res[a];
        res.erase(res.begin() + a);

        a = rand() % res.size();
        GTCC r = res[a];
        res.erase(res.begin() + a);

        res.push_back(GTCC(joinspclusters(l.c,r.c), string("(" + l.s + "," + r.s + ")"))); // genrooted

        deletespcluster(l.c);
        deletespcluster(r.c);

    }    

    vs.push_back(res[0]);      
  }

  // Ugly
  while (vs.size()>1)
    {
        int a = rand()%vs.size();
        GTCC l = vs[a];
        vs.erase(vs.begin() + a);

        a = rand() % vs.size();
        GTCC r = vs[a];
        vs.erase(vs.begin() + a);

        vs.push_back(GTCC(joinspclusters(l.c,r.c), string("(" + l.s + "," + r.s + ")"))); // genrooted

        deletespcluster(l.c);
        deletespcluster(r.c);
    }    

  
  char *s = strdup(vs[0].s.c_str());
  RootedTree *r = new RootedTree(s); 
  free(s);


  return r; 

}

class LocalSearch;
class LSSetting;
TotalCost initializeLS(vector<LocalSearch*> &localsearch, GTS &gtvec,  RootedTree *s, int &coststruct);

/********************************************************************
 ********************************************************************
 ********************************************************************/


class LocalSearch {
public:
    
  int repeats; // 0 - until better is not found; >0 - number of cycles
  
  LocalSearch(int _repeats)  { if (!_repeats) repeats=-1; else repeats=_repeats;}
  virtual int explore(RootedTree *s,LSSetting &ls)=0;
  virtual char * name()=0;
  //virtual DlCost costinit(GTS &gtvec,RootedTree *s,int first, int initstruct)=0;
  virtual int  coststruct()=0;
  virtual int reinitGTS() { return 1; } // if GTS is destroyed 
  virtual int needGTS() { return 1; } // if GTS is destroyed 
};


class LSSetting
{
public:
  int run;  
  int generation;
  GTS &gtvec;
  RootedTree *s;
  COSTTYPE curopt;
  int tproc, tnew;
  long cntrec;
  int lastopt;
  int lastopt2;
  int coststruct;
  ArrTab *at;
  TotalCost total;
  int destroyedGTS;
  vector<LocalSearch*> &localsearch;
  TreeRepr *tr;
  int improvecnt;
  int lsnum;
  int state;
  int preserveroot; 

  LSSetting(GTS &_gtvec,vector<LocalSearch*> &_localsearch, int _preserveroot) : run(0), generation(0), gtvec(_gtvec), curopt(0), tproc(0), tnew(0), cntrec(0),
      lastopt(0), at(NULL), total(TotalCost(0,0)), 
      localsearch(_localsearch), lsnum(0), state(0),preserveroot(_preserveroot)
      {
      }

  void initst(RootedTree *_s, vector<LocalSearch*> &localsearch)
  {      
      at=new ArrTab;    
      s = _s;
      total=initializeLS(localsearch,gtvec,s,coststruct);      
      curopt=total.cost;      
      at->add(s->repr(), s->reprlen(), total.cost, tr, 0, 0);
      at->add2waiting(tr);
      destroyedGTS=0;
  }


  int replacesttree()
  {
        tr = at->getwaiting();
        if (!tr) return 0;
        s->replace(tr);    
        total=s->initGTS(gtvec,0,1);         
        destroyedGTS=0; 
        verb_smp << endl;
        cout << "!" << total.cost;
        return 1;
    }

  int singlelscycle()
  {
    TreeRepr *cur=tr;
    int fnd=0;
    improvecnt=0;
    for (int lscycle=0;lscycle<localsearch.size();lscycle++)
    {        
      LocalSearch *locsear=localsearch[lscycle];  
      if (destroyedGTS && locsear->needGTS()) { 
            total=s->initGTS(gtvec,0,1); 
            destroyedGTS=0; 
      }

      int lsi=locsear->repeats;
      int curproc=-1;;
      while (lsi) 
      {        
        cur->status = TRPROCESSED;
        if (curproc>1000 || curproc==-1)
        {        
          verb_smp << endl;
          if (lsnum>0) cout << lsnum << ". ";
          verb_smp << "[" << locsear->name() << "] " << cur->cost;
          verb_smp << "/" << at->optimal() << " ";
          curproc=0;
        }
        
        tnew=0;
        tproc=0;        
        lsi--;

        fnd = locsear->explore(s,*this);        
        //cout << " (" << tproc << " processed/" << tnew << " new trees)";            
        curproc+=tproc;
        if (fnd) 
        {
            improvecnt++;
            verb_smp << "+";
            //cout << " new optimal found :)" << endl;
            cur = at->getwaiting();            
        }
        else { 
            verb_smp << "-"; //" no improvement" << endl;        
            break;            
        }
      } // while(1)

      destroyedGTS=destroyedGTS || locsear->reinitGTS();      
    } // for     
    return improvecnt;
  } // singlelscycle
};


/********************************************************************
 ********************************************************************
 ********************************************************************/

class LocalSearchNNI : public LocalSearch
{
public:
  
  LocalSearchNNI(int _repeats) : LocalSearch(_repeats) {}

  virtual int reinitGTS() { return 0; }

  virtual int  coststruct() { return COSTGTS; }
  virtual char * name() { return (char*)"NNI"; }

  virtual int explore(RootedTree *s,LSSetting &ls)  
  {
    SPID trepr[s->reprlen()];
    string info;
    int d;
    TreeRepr *tr;
    int in = s->nn - s->lf;
    int lastopt = ls.lastopt;
    TotalCost totaldelta(0,0);
    int cnt=0;    


 

    for (int k = 0; k < in; k++) {
    int n = s->lf + (lastopt + k) % in;



      if (ls.preserveroot and n==s->root) 
        {
          //cout << "Root!"<< endl;
          continue;
        }

    for (int lr = 0; lr < 2; lr++)
    {

      COSTTYPE oldcost = ls.total.cost;

      //pcct(-14,ls.total);

#define NNICYCLE 3
      for (int c = 0; c < NNICYCLE; c++)
      {
#define RR cout << cnt << "." << c << " ";

        //cout << n << " " << lr << " " << c << endl;
        

        int nnires = s->nni(n, lr, ls.gtvec, totaldelta, d, info);
        pcct(-15,totaldelta);        
        
        if (!nnires) continue;

        ls.tproc++;
        tsprocstats;
        cnt++;        
        ls.total.update(totaldelta);

        //pcct(-15,ls.total);

        if (c == NNICYCLE-1) {        
          if (oldcost != ls.total.cost)
          {
            s->printdeb(cout,0);
#ifdef NNIDUMP            
            s->dumpnnistructs(ls.gtvec, -1, cnt);
#endif            
            cout << endl;
            cout << "Cost err " << oldcost << " " << ls.total.cost << endl;
            cout << *s << endl;
            cout << s->initGTS(ls.gtvec,0,1).cost << endl;
            ls.at->save();
            exit(-1);
          }
          continue;
        }
        COSTTYPE curcost = ls.total.cost;


#ifdef DEBUGREC3
        RR;
        cout << " " << n << " " << lr << " " << c
             << " curcost=" << curcost << " opt=" << ls.at->opt << "]" << endl;
#endif

        int newopt = curcost < ls.curopt;
        int res = ls.at->add(s->repr(trepr), s->reprlen(), curcost, tr, ls.generation, n);

	
#ifdef DEBUGREC3
        cout << "  " << *tr << " " << curcost << " ";
        RR;
        cout << endl;
#endif

        if (res == 4)
        {
          s->dumpnnistructs(ls.gtvec, -1, cnt);
          cout << "Ooops " << endl;
          cout << *s  << " total="
               << ls.total.cost << " totaldelta=" << totaldelta <<  "  " << info << endl;
          ls.at->save();
          exit(-1);
        }

        if (newopt)
        {
          // better found
          // cout << "New optimal " << curcost  << endl; // << endl;

#ifdef DEBUGREC3
          cout << *s << endl;
          cout << *tr << endl;
#endif
          ls.at->add2waiting(tr);
          ls.curopt = curcost;
          ls.lastopt=n;
          return 1;
        }

        if (tr->status == TRPROCESSED)
        {
#ifdef DEBUGREC3
          cout << "PROCESSED" << endl << endl;
#endif
          continue;
        }

#ifdef DEBUGREC3
        cout << endl;
#endif
        if (res) {
          ls.tnew++;
          ls.at->add2waiting(tr);
        }
      } // if nnires
    }
  }
  return 0;

  }
  
  };


/********************************************************************
 ********************************************************************
 ********************************************************************/


class LocalSearchSPRN : public LocalSearch
{
public:
  
  LocalSearchSPRN(int _repeats) : LocalSearch(_repeats) {}

  virtual int reinitGTS() { return 0; }

  //virtual DlCost costinit(GTS &gtvec,RootedTree *s,int first, int initstruct)
  virtual int coststruct() { return COSTGTS; }
  //{ return s->initGTS(gtvec, first, initstruct); }  
  virtual char * name() { return (char*)"SPRn"; }

  virtual int explore(RootedTree *s,LSSetting &ls)  
  {
    SPID trepr[s->reprlen()];
    string info;
    int d;
    TreeRepr *tr;
    int in = s->nn - s->lf;
    int lastopt = ls.lastopt;
    TotalCost totaldelta(0,0);
    int cnt=0;    
    COSTTYPE oldcost = ls.total.cost;

    for (int i = 0; i < s->nn; i++)
        { 
            SPID ip=(ls.lastopt+i)%s->nn;
            // ip - source subtree 
            if (ip==s->root) continue;                        
            _gsetrees=100*ip;

            SPID curroot=s->root;

            if (ls.preserveroot)
            {
              // find child of the root
              curroot=ip;
              while (s->par[curroot]!=s->root) curroot=s->par[curroot];
              if (ip==curroot) continue;               
            }
            
            int traversecnt=(s->subtreesize(curroot)-s->subtreesize(ip))*3-3;
            int ipleftchild=s->isleftchild(ip);
#ifdef SPRNDEBUG                    
            cout << endl << endl << traversecnt << "." << ip << " SPRN-INIT" << endl;
            s->printdeb(cout,1) << endl;
#endif            

            while (traversecnt--)
            {            
                                
                int nnires = s->nnirot(ip, ls.gtvec, totaldelta, d, info, curroot);                
                if (!nnires) { 
                    cout << traversecnt << " NO RESULT " << ip << endl;
                    break;    
                }


#ifdef SPRNDEBUG                      
                cout << traversecnt << "." << ip << " SPRN-AFTER " << ip << endl;
                s->printdeb(cout,1,to_string(nnires)+"."+to_string(traversecnt)) << endl;
#endif

                ls.tproc++;
                tsprocstats;
                cnt++;
                ls.total = ls.total+totaldelta;


                COSTTYPE curcost = ls.total.cost;

                int newopt = curcost < ls.curopt;
                int res = ls.at->add(s->repr(trepr), s->reprlen(), curcost, tr, ls.generation, ip);

                if (res == 4)
                {
                  s->dumpnnistructs(ls.gtvec, -1, cnt);
                  cout << "Ooops " << endl;
                  cout << *s  << " total="
                       << ls.total.cost << " totaldelta=" << totaldelta <<  "  " << info << endl;
                  ls.at->save();
                  exit(-1);
                }

                if 
#ifdef SPRNDEBUG        
                (0) 
#else              
                (newopt)
#endif                
                {
                  // better found
                  // cout << "New optimal " << curcost  << endl; // << endl;
        #ifdef DEBUGREC3
                  cout << *s << endl;
                  cout << *tr << endl;
        #endif
                  ls.at->add2waiting(tr);
                  ls.curopt = curcost;
                  ls.lastopt=ip;
                  return 1;
                }

                if (tr->status == TRPROCESSED)
                {
        #ifdef DEBUGREC3
                  cout << "PROCESSED" << endl << endl;
        #endif
                  continue;
                }

        #ifdef DEBUGREC3
                cout << endl;
        #endif

                if (res) {
                  ls.tnew++;
                  ls.at->add2waiting(tr);
                }
          
            } // while (traversecnt--)

#ifdef SPRNDEBUG            
            s->printdeb(cout,2) << endl;
            cout << "!!!" << ip << ": " << ipleftchild << " " << s->isleftchild(ip);
#endif            
            if (ipleftchild != s->isleftchild(ip)) s->swapchildren(s->getparent(ip));
            
      } // for (i=...)

  return 0;
}
 
  
 };




class LocalSearchSPRNPresRoot : public LocalSearch
{
public:
  
  LocalSearchSPRNPresRoot(int _repeats) : LocalSearch(_repeats) {}

  virtual int reinitGTS() { return 0; }

  //virtual DlCost costinit(GTS &gtvec,RootedTree *s,int first, int initstruct)
  virtual int coststruct() { return COSTGTS; }
  //{ return s->initGTS(gtvec, first, initstruct); }  
  virtual char * name() { return (char*)"SPRn"; }

  virtual int explore(RootedTree *s,LSSetting &ls)  
  {
    SPID trepr[s->reprlen()];
    string info;
    int d;
    TreeRepr *tr;
    int in = s->nn - s->lf;
    int lastopt = ls.lastopt;
    TotalCost totaldelta(0,0);
    int cnt=0;    
    COSTTYPE oldcost = ls.total.cost;

    for (int i = 0; i < s->nn; i++)
        { 
            SPID ip=(ls.lastopt+i)%s->nn;
            if (ip==s->root) continue;            
            // ip - source subtree 

            // find child of the root
            SPID artroot=ip;
            while (s->par[artroot]!=s->root) artroot=s->par[artroot];
            if (ip==artroot) continue;
            
            _gsetrees=100*ip;

            int traversecnt=(s->subtreesize(artroot)-s->subtreesize(ip))*3-3;

            int ipleftchild=s->isleftchild(ip);
#ifdef SPRNDEBUG                    
            cout << endl << endl << traversecnt << "." << ip << " SPRN-INIT" << endl;
            s->printdeb(cout,1) << endl;
#endif            



            while (traversecnt--)
            {            
                                
                int nnires = s->nnirot(ip, ls.gtvec, totaldelta, d, info, artroot);                
                if (!nnires) { 
                    cout << traversecnt << " NO RESULT " << ip << endl;
                    break;    
                }


#ifdef SPRNDEBUG                      
                cout << traversecnt << "." << ip << " SPRN-AFTER " << ip << endl;
                s->printdeb(cout,1,to_string(nnires)+"."+to_string(traversecnt)) << endl;
#endif

                ls.tproc++;
                tsprocstats;
                cnt++;
                ls.total = ls.total+totaldelta;


                COSTTYPE curcost = ls.total.cost;

                int newopt = curcost < ls.curopt;
                int res = ls.at->add(s->repr(trepr), s->reprlen(), curcost, tr, ls.generation, ip);

                if (res == 4)
                {
                  s->dumpnnistructs(ls.gtvec, -1, cnt);
                  cout << "Ooops " << endl;
                  cout << *s  << " total="
                       << ls.total.cost << " totaldelta=" << totaldelta <<  "  " << info << endl;
                  ls.at->save();
                  exit(-1);
                }

                if 
#ifdef SPRNDEBUG        
                (0) 
#else              
                (newopt)
#endif                
                {
                  // better found
                  // cout << "New optimal " << curcost  << endl; // << endl;
        #ifdef DEBUGREC3
                  cout << *s << endl;
                  cout << *tr << endl;
        #endif
                  ls.at->add2waiting(tr);
                  ls.curopt = curcost;
                  ls.lastopt=ip;
                  return 1;
                }

                if (tr->status == TRPROCESSED)
                {
        #ifdef DEBUGREC3
                  cout << "PROCESSED" << endl << endl;
        #endif
                  continue;
                }

        #ifdef DEBUGREC3
                cout << endl;
        #endif

                if (res) {
                  ls.tnew++;
                  ls.at->add2waiting(tr);
                }
          
            } // while (traversecnt--)

#ifdef SPRNDEBUG            
            s->printdeb(cout,2) << endl;
            cout << "!!!" << ip << ": " << ipleftchild << " " << s->isleftchild(ip);
#endif            
            if (ipleftchild != s->isleftchild(ip)) s->swapchildren(s->getparent(ip));
            
      } // for (i=...)

  //exit(0);
  return 0;
}
 
  
 };

  /**************************************/
/**************************************/
  /**************************************/
class LocalSearchSPR : public LocalSearch
{
public:
  
  LocalSearchSPR(int _repeats) : LocalSearch(_repeats) {}

  virtual int needGTS() { return 0; } 

  // virtual DlCost costinit(GTS &gtvec,RootedTree *s,int first, int initstruct)
  // { return s->gtccost(gtvec); }  

  virtual int coststruct() { return COSTGTC; }

  virtual  char * name() { return (char *)"SPR"; }


  virtual int attachandtest(LSSetting &ls, RootedTree *s, SPID i, SPID c, SPID sib)
      {

#ifdef SPRDEBUG
      cout << "---------- SRC DETACH --------- " << i << " " << s->root << " ";
      s->printdeb(cout);

      SPID *par = spidcopy(s->par, s->nn);
      SPID *child = spidcopy(s->child, s->nn);
      SPID root = s->root;
      // cout << "========================SPR " << i << " " << sib << "::";
      // for (int j=0;j<cnt;j++) cout << cand[j] << " ";
      // cout << endl;
#endif


        SPID trepr[s->reprlen()];
        TreeRepr *tr;
#ifdef SPRDEBUG
          //cout << i << " " << j << endl;
#endif
          s->sprattach(i, c);

          // Cost compute!
          ls.total = s->gtccost(ls.gtvec);
          ls.tproc++;  
          tsprocstats;

#ifdef SPRDEBUG
          cout << "----------------after attach" << i << " " << c << endl;
          s->printdeb(cout);
          if (s->clustersize(s->root) != s->lf)
          {
            cout << "ROOT!" << endl;
            exit(-1);
          }
#endif
          COSTTYPE curcost = ls.total.cost;

          int newopt = curcost < ls.curopt;
          int res = ls.at->add(s->repr(trepr), s->reprlen(), curcost, tr, ls.generation,0);

          if (newopt)
          {
#ifdef SPRDEBUG2
            cout << endl << curcost << "NEWOPT" << endl;
#endif
            ls.lastopt=sib;
            ls.lastopt2=c;
            cout << "  spr OPT" << i << " " << sib << " " << c << " dist=" << s->nodedist(sib,c) << " ";            
            ls.at->add2waiting(tr);
            ls.curopt = curcost;
            return 1;
          }

          if (tr->status == TRPROCESSED)
          {
#ifdef SPRDEBUG
            //cout << "PROCESSED" << endl << endl;
#endif
          }
          else {
#ifdef SPRDEBUG
            //cout << endl;
#endif
            if (res) {
              ls.tnew++;
              ls.at->add2waiting(tr);
            }
          }
          SPID _x;
          s->sprdetach(i, _x);
#ifdef SPRDEBUG
          cout << "--should be SRC DETACH ------------------" << i << endl;
          s->printdeb(cout);

#define printspid(a) for (int k=0;k<=s->nn; k++) cout << a[k] << " ";

          //check
          int ok = -1;
          for (int z = 0; z < s->nn; z++)
            if (s->par[z] != par[z])
            { ok = 0; cout << "PAR ERR: " << z << " " << par[z] << " " << s->par[z] << endl; }
          for (int z = 0; z < s->nn; z++)
            if (s->child[z] != child[z])
            { ok = 0; cout << "CHI ERR: " << z << " " << child[z] << " " << s->child[z] << endl; }
          if (root != s->root)
          { ok = 0; cout << "ROO ERR: " << root << " " << s->root << endl; }
          if (!ok)
          {
            printspid(par);   cout << endl;
            printspid(child);             cout << endl;
            cout << "ROOT " << root << " " << s->root << endl;
            exit(-1);
          }
#endif
          return 0;
        }

     

  virtual int explore(RootedTree *s,LSSetting &ls)              
  {
    
    string info;
    int d;
    
    int in = s->nn - s->lf;

    //s->printdeb(cout);

    SPID cand[s->nn];    
    //s->printdeb(cout);

#ifdef SPRDEBUG
    if (s->clustersize(s->root)   != s->lf)
    {
      cout << "ROOT!" << endl;
      exit(-1);
    }

#endif

    //int startoffset = rand() % s->nn;    
    int startoffset=ls.lastopt;
    for (int ip = 0; ip < s->nn; ip++)
    {
      // randomize start
      int i = (ip + startoffset) % s->nn;

      if (i == s->root) continue;


      SPID sib, _x;
      int xcnt = 0;

#ifdef SPRDEBUG
      cout << "========= INIT TREE ===========" << i << " ";
      s->printdeb(cout);
#endif

      s->sprdetach(i, sib);
      s->subtreenodes(s->root, cand, xcnt);



      for (int j = 0; j < xcnt; j++)
        if (j != sib) 
            if (attachandtest(ls,s,i,cand[j],sib)) return 1;
        
      s->sprattach(i, sib);
#ifdef SPRDEBUG
      cout << "--should be INIT  ------------------" << i << endl;
      s->printdeb(cout);
#endif

    }
    return 0;
  }

};


class LocalSearchSPRDist : public LocalSearchSPR
{
public:

    int exploredist;

    LocalSearchSPRDist(int _repeats, int _exploredist=6) : LocalSearchSPR(_repeats), exploredist(_exploredist)
    {}

    virtual  char * name() { return (char *)"SPRd"; }


  virtual int explore(RootedTree *s,LSSetting &ls)              
  {
    
    string info;
    int d;
    
    int in = s->nn - s->lf;

    //s->printdeb(cout);

    SPID cand[s->nn];    
    //s->printdeb(cout);

#ifdef SPRDEBUG
    if (s->clustersize(s->root)   != s->lf)
    {
      cout << "ROOT!" << endl;
      exit(-1);
    }

#endif

    //int startoffset = rand() % s->nn;    
    int startoffset=ls.lastopt;
    for (int ip = 0; ip < s->nn; ip++)
    {
      // randomize start
      int i = (ip + startoffset) % s->nn;

      if (i == s->root) continue;


      SPID sib, _x;
      int xcnt = 0;

#ifdef SPRDEBUG
      cout << "========= INIT TREE ===========" << i << " ";
      s->printdeb(cout);
#endif

      s->sprdetach(i, sib);
      s->subtreenodes(s->root, cand, xcnt);



      for (int j = 0; j < xcnt; j++)
        if (j != sib && s->nodedist(i,cand[j])<exploredist)
            if (attachandtest(ls,s,i,cand[j],sib)) return 1;
        
      s->sprattach(i, sib);
#ifdef SPRDEBUG
      cout << "--should be INIT  ------------------" << i << endl;
      s->printdeb(cout);
#endif

    }
    return 0;
  }

};






/********************************************************************
 ********************************************************************
 ********************************************************************/


class LocalSearchTSW : public LocalSearch
{
public:
  
  LocalSearchTSW(int _repeats) : LocalSearch(_repeats) {}

  virtual int needGTS() { return 0; } 

  // virtual DlCost costinit(GTS &gtvec,RootedTree *s,int first, int initstruct)
  // { return s->gtccost(gtvec); }  
  virtual int coststruct() { return COSTGTC; }

  virtual  char * name() { return (char *)"TSW"; }


  virtual int explore(RootedTree *s,LSSetting &ls)              
  {
    SPID trepr[s->reprlen()];
    string info;
    int d;
    TreeRepr *tr;
    int in = s->nn - s->lf;

    //s->printdeb(cout);

    SPID cand[s->nn];    

    int startoffset=ls.lastopt;
    for (int ip = 0; ip < s->nn; ip++)
    {
      // randomize start
      int i = (ip + startoffset) % s->nn;
      ls.lastopt=i;

      if (i == s->root) continue;
      SPID sib, _x;
      int xcnt = 0;

#ifdef TSWDEBUG
      cout << endl << endl << "========= INIT TREE ===========" << i << " ";
      s->printdeb(cout);
      SPID *par = spidcopy(s->par, s->nn);
      SPID *child = spidcopy(s->child, s->nn);
      SPID root = s->root;   

      for (int o=0;o<s->nn;o++) if (s->par[o]==o) exit(-1);
#endif
      s->rightsubtrecandidates(i, cand, xcnt, ls.preserveroot);

#ifdef TSWDEBUG
      cout << ip << "CAND=";
      for (int o=0;o<xcnt;o++) cout << cand[o] << ".";
      cout << endl;
#endif

      for (int j = 0; j < xcnt; j++)
        if (s->swapnodes(i,cand[j]))
        {
          SPID sib2;          



#ifdef TSWDEBUG
      cout << "---------- AFTER SWAP  --------- " << i << "<-->" << cand[j];
      s->printdeb(cout);   
      for (int o=0;o<s->nn;o++) if (s->par[o]==o) exit(-1);
#endif          

          // Compute cost!
          ls.total = s->gtccost(ls.gtvec);
          ls.tproc++;  
          tsprocstats;
          
          COSTTYPE curcost = ls.total.cost;

          int newopt = curcost < ls.curopt;
          int res = ls.at->add(s->repr(trepr), s->reprlen(), curcost, tr, ls.generation,0);

          if (newopt)
          {            
            ls.lastopt=sib;
            ls.lastopt2=cand[j];
           // cout << "  tsw OPT" << i << " " << " " << cand[j] << " dist=" << s->nodedist(i,cand[j]) << " ";
            ls.at->add2waiting(tr);
            ls.curopt = curcost;
            return 1;
          }

          if (tr->status == TRPROCESSED)
          {
#ifdef TSWDEBUG
            //cout << "PROCESSED" << endl << endl;
#endif
          }
          else {

#ifdef TSWDEBUG
            //cout << endl;
#endif
                if (res) {
                  ls.tnew++;
                  ls.at->add2waiting(tr);
                }
              }

            s->swapnodes(i,cand[j]);
            
#ifdef TSWDEBUG
          cout << "--should be INIT  ------------------" << i << endl;
          s->printdeb(cout);
          for (int o=0;o<s->nn;o++) if (s->par[o]==o) exit(-1);

#define printspid(a) for (int k=0;k<=s->nn; k++) cout << a[k] << " ";

          //check
          int ok = -1;
          for (int z = 0; z < s->nn; z++)
            if (par[z] != s->par[z])
            { ok = 0; cout << "PAR ERR: " << z << " " << par[z]<< " " << s->par[z] << endl; }
          for (int z = 0; z < s->nn; z++)
            if (child[z] != s->child[z])
            { ok = 0; cout << "CHI ERR: " << z << " " << child[z] << " " << s->child[z] << endl; }
          if (root != s->root)
          { ok = 0; cout << "ROO ERR: " << root << " " << s->root << endl; }
          if (!ok)
          {
            printspid(par);   cout << endl;
            printspid(child);             cout << endl;
            cout << "ROOT " << root << " " << s->root << endl;
            exit(-1);
          }
#endif            
        }
      
#ifdef TSWDEBUG
      cout << "--should be INIT  ------------------" << i << endl;
      s->printdeb(cout);
#endif
    }
    return 0;
  }

};



void mixexplore(STS &st, GTS &gtvec, vector<LocalSearch*> &localsearch, int save, int preserveroot);


void runstarheur(GTS &gtvec, vector<LocalSearch*> &localsearch, int preserveroot)
{

    vector<int> speciescollection;

    SPID initcoll[3];
    RootedTree *st=NULL; //new RootedTree(strdup(genrandomtree(s, 3).c_str()));
    int cnt=0;
    for (int i=0;i<specnames2id.size();i++)
        speciescollection.push_back(i);
    // cout << *st << endl


    while (speciescollection.size()>0)
    {
        // take new species
        int rs=rand()%speciescollection.size();
        //cout << rs << endl;
        SPID curspecies=speciescollection[rs];

        speciescollection.erase(speciescollection.begin()+rs);

        
        if (cnt<3) 
            { 
                initcoll[cnt++]=curspecies;
                continue;                
            }
        
        if (cnt==3)
        {

            st=new RootedTree(strdup(genrandomtree(initcoll, 3).c_str()));
            cnt++;
            cout << *st << endl;            
        }
        // normal iteration

        std::stringstream b; 
        b << *st;
        string s = "("+species(curspecies)+ "," + b.str() + ")";
                    
        RootedTree *stn=new RootedTree(strdup(s.c_str()));
        delete st;
        st=stn;
    
        


        // Create new collection of gene trees (contracted)
        GTS cc;
        if (speciescollection.size()>0)
        {
          for (int i=0;i<gtvec.size();i++)
          { 
              string s=gtvec[i]->contracted(0,st,1);
              //cout << s << endl;
              if (!s.empty())   
              {  
                  UnrootedTree *u = new UnrootedTree(strdup(s.c_str()));
                  if (u->it>0) cc.push_back(u);
                  else delete u;
              }
          }
        }
        else 
          {
            for (int i=0;i<gtvec.size();i++) cc.push_back(gtvec[i]);
          }

        STS stvec;
        stvec.push_back(st);
        // run heuristics
        // TODO: preserve root not implemented here!

        if (preserveroot)
        {
          cout << "Preserve Root is not implemented in star heuristics" << endl;
          exit(2);
        }

        mixexplore(stvec,cc,localsearch,speciescollection.size()==0,0);

        // Clean after 
        if (speciescollection.size()>0)
          for (int i=0;i<cc.size();i++) delete cc[i];

    }  
}


TotalCost initializeLS(vector<LocalSearch*> &localsearch, GTS &gtvec,  RootedTree *s, int &coststruct)
{
  TotalCost total(0,0);
  for (int i=0;i<localsearch.size();i++)      
    coststruct|=localsearch[i]->coststruct();  
  if (!coststruct || coststruct & COSTGTS) 
    total=s->initGTS(gtvec,1,1);
  if (coststruct & COSTGTC) 
    total=s->initGTS(gtvec,1,1);  
  pcct(-2,total);
  return total;
}




class STClusters
{  
  map<SPID*, int, comparespids> t;

  public:
  STClusters() {}

  ~STClusters() {
    map<SPID*,int,comparespids>::iterator it;
    for (it = t.begin(); it != t.end(); ++it)    
        deletespcluster((*it).first);    
  }

  void addst(TreeRepr *tr)
  {
    RootedTree *rt=new RootedTree(strdup(to_string(*tr).c_str()));
    SPID **sprepr=rt->getspclusterrepr();
    for (int i=rt->lf;i<rt->nn;i++)
        // printspcluster(cout,sprepr[i]);
    {
        map<SPID*,int,comparespids>::iterator it=t.find(sprepr[i]);
        if (it == t.end())       
          t[sprepr[i]]=1;
        else { 
          t[sprepr[i]]++;    
          deletespcluster(sprepr[i]);
        }
    }
    
  }

  void print()
  {
    map<SPID*,int,comparespids>::iterator it;
    for (it = t.begin(); it != t.end(); ++it)
    {
        SPID *ar= (*it).first;
        //printspcluster(cout,(*it).first);
        if ((*it).second>5)
        { for (int i=1;i<=ar[0];i++) cout << specnames[ar[i]] << " ";
          cout << "=" << (*it).second << endl; 
        }
    }
    cout << endl;
  }
};



void mixexploremulti(STS &st, GTS &gtvec, vector<LocalSearch*> &localsearch, int save, int preserveroot)
{

  vector<LSSetting*> lst;
  TreeRepr *tr;
  int coststruct;

  //cout << "MULTI! n=" << st.size() << endl;

  COSTTYPE curopt,curworst;
  int curopti,curworsti;
  for (int sti=0; sti<st.size();sti++)
  {   
      LSSetting *ls =  new LSSetting(gtvec,localsearch,preserveroot);
      
      lst.push_back(ls);
      ls->initst(st[sti],localsearch);      
      if (sti==0 || curopt>ls->curopt) {         
        curopt=ls->curopt;
        curopti=sti;      
        coststruct=ls->coststruct;        
      }      
      if (sti==0 || curworst<ls->curopt) {         
        curworst=ls->curopt;
        curworsti=sti;              
      }      
      if (st.size()>1) ls->lsnum=sti+1; 
      lst[sti]->destroyedGTS=1;      
  }    

  int oldcurwosti=curworsti;
  while (1)
  {
      
      if (oldcurwosti!=curworsti)
        lst[curworsti]->destroyedGTS=1;  

      if (!lst[curworsti]->singlelscycle())
      {        
        lst[curworsti]->state=1;
        //lst[curworsti]->replacesttree();
      }
      
      oldcurwosti=curworsti;
      curworsti=-1;
      curopti=0;
      for (int i=0; i<st.size();i++)
      {      
        if (lst[curopti]->curopt>=lst[i]->curopt) curopti=i;              
        if (lst[i]->state==1) continue;
        if (curworsti==-1) curworsti=i;
        if (lst[curworsti]->curopt<lst[i]->curopt) curworsti=i;                    
      }      
      if (curworsti==-1) break;

      verb_smp << endl;

      STClusters stc;
      for (int sti=0; sti<st.size();sti++)
      {
        verb_smp << sti << ".";
        if (lst[sti]->state) { verb_smp << "-"; } else { verb_smp << "+"; }
        verb_smp << " " << lst[sti]->at->opt;
        TreeRepr *trepr = lst[sti]->at->getoptimaltree();
        if (trepr) verb_det << " " << *trepr;
        verb_smp << endl;
        stc.addst(trepr);
      }
      if (verbose>=VERBOSE_DETAILED) stc.print();
  
      int eq=0;
      for (int i=0;i<st.size();i++)
        if (lst[i]->curopt==curopt) eq++;
      
      //if (eq==st.size() || eq>st.size()/2) break;

  }


  for (int sti=0; sti<st.size();sti++)
    verb_det << sti << ". " << lst[sti]->curopt << " " 
        << *lst[sti]->at->getoptimaltree() << endl;
   

  string fn=lst[0]->at->filetosave();


  // Merge results into one
  
  verb_det << "Merging " << st.size() << " result queues"<< endl;
  ArrTab at;

  for (int i=0; i<st.size();i++)
      lst[i]->at->insertinto(&at);

  at.save(0, fn.c_str());

  cout << fn << " saved" << endl;

  for (int i=0; i<st.size();i++)
      delete lst[i];

  // if (save) { 
  //   at.save();
  //   cout << "OK. Search completed" << endl;
  //   cout << "Number of edit operations:" << cnt << endl;
  //   cout << "Unique trees:" << at.size << endl;
  //   cout << "Optimal cost:" << at.optimal() << endl;
  //   cout << "Number of optimal trees:" << at.optnum << endl;
  // }
  // else
  // {
  //     //insert opt into s
  //     cout << "OK. Search completed" << endl;
  //     cout << "Optimal cost:" << at.optimal() << endl;
  //     s->replace(at.getoptimaltree()); 
  // }
}



void mixexplore(STS &st, GTS &gtvec, vector<LocalSearch*> &localsearch, int save, int preserveroot)
{

  LSSetting ls(gtvec,localsearch,preserveroot);  


  TreeRepr *tr;
  ArrTab at;

  ls.at=&at;
  
  int curtree = 0;
  RootedTree *s = st[curtree];
  int coststruct=0;

  ls.total=initializeLS(localsearch,gtvec,s,coststruct);
  
  ///////////
  int runs = 1;
  int cnt = 1;


  pcct(0,ls.total);
  
  at.add(s->repr(), s->reprlen(), ls.total.cost, tr, 0, 0);
  at.add2waiting(tr);
  TreeRepr *cur = tr;
  ls.curopt = ls.total.cost;

  
  
  int destroyedGTS=0;
  while (curtree < (int)st.size())
  {
    int bigruns = mixexplorebigsteps; 
    while (bigruns)
    {
        pcct(2,ls.total);
        pcc(2,cur->cost);

        if (!cur) {
            verb_det << "No more new trees..." << endl;
            break;
        }    
        int fnd=1;
        while (fnd)
        {
            for (int lscycle=0;lscycle<localsearch.size();lscycle++)
            {        

                

              pcct(3,ls.total);
              pcc(3,cur->cost);

                LocalSearch *locsear=localsearch[lscycle];  
                if (destroyedGTS && locsear->needGTS()) { 
                    ls.total=s->initGTS(gtvec,0,1); 

                    destroyedGTS=0; 
                }

                
                int lsi=locsear->repeats;
                while (lsi)
                {                        
                    
                    //pcct(4,ls.total); pcc(4,cur->cost);

                    cur->status = TRPROCESSED;
                    verb_smp << bigruns << "/" << runs << "/" << lsi << ". [" << locsear->name() << "]" << cur->cost << "[" << cur->generation << "]" << "/" << ls.at->optimal() << "/" << ls.curopt << ". Searching";        
                    ls.tnew=0;
                    ls.tproc=0;        
                    lsi--;

                    
                    if (verbose>=VERBOSE_DETAILED) at.printwaiting();                    
                    fnd = locsear->explore(s,ls);                    
                    if (verbose>=VERBOSE_DETAILED) at.printwaiting();                    
                    
                    runs++;

                    verb_smp << " (" << ls.tproc << " processed/" << ls.tnew << " new trees)";            
                    if (fnd) 
                    {
                        verb_smp << " new tree :)" << endl;
                        cur = at.getwaiting();
                        bigruns = mixexplorebigsteps;
                    }
                    else { 
                        verb_smp << " no improvement" << endl;        
                        break;            
                    }
                }
                destroyedGTS=destroyedGTS || locsear->reinitGTS();
            }
        }
                  
        bigruns--;
        if (!bigruns) break;
        // jumping into the next
        cur = at.getwaiting();
        if (!cur) break;
        s->replace(cur);    
        if (!coststruct || coststruct & COSTGTS) ls.total=s->initGTS(gtvec,0,1); 
        if (coststruct & COSTGTC) ls.total=s->gtccost(gtvec);
        destroyedGTS=0;    
    }   // bigrun

    while (1)
    {
      curtree++;
      if (curtree == (int)st.size()) break;
      TreeRepr ntr(st[curtree]->repr(), s->reprlen(), 0, curtree, 0);
      s->replace(&ntr);    
      if (!coststruct || coststruct & COSTGTS) ls.total=s->initGTS(gtvec,0,1); 
      if (coststruct & COSTGTC) ls.total=s->gtccost(gtvec);
      destroyedGTS=0;       
      ls.curopt = ls.total.cost;
      int res = at.add(s->repr(), s->reprlen(), ls.curopt, tr, curtree, 0 );
      verb_smp << "Initializing next species tree" << endl;
      at.add2waiting(tr);
      cur = tr;
      break;
    }
  } 

  
  if (save) { 
    at.save();
    verb_det << "OK. Search completed" << endl;
    verb_det << "Number of edit operations:" << cnt << endl;
    verb_det << "Unique trees:" << at.size << endl;
    verb_smp << "Optimal cost:" << at.optimal() << endl;
    verb_smp << "Number of optimal trees:" << at.optnum << endl;
    verb_cntcost << at.optimal() << " " << at.optnum <<  endl;
    
  }
  else
  {
      //insert opt into s
      verb_det << "OK. Search completed" << endl;
      verb_smp << "Optimal cost:" << at.optimal() << endl;
      s->replace(at.getoptimaltree()); 
  }
}


void RootedTree::dumpnnistructs(GTS &g, int gtree, int num) // for comparing DEBUGs
{
  ofstream s;
  char buf[200];

  if (num > 0) sprintf(buf, "%03d.nni.%02d.dump", num, dumpcnt);
  else sprintf(buf, "nni.%02d.dump", dumpcnt);

  cout << "Dump nnistructs " << ++dumpcnt << " " << buf << endl;

  saveraw(string(buf) + ".raw");
  s.open(buf);
  int i, j;

  s << "#Species tree" << endl;
  s << *this << endl;

  s << "#Species" << endl;
  for (i = 0; i < (int)specnames.size(); i++)
    s << i << " " << specnames[i] << endl;

  s << "#Number of gene trees" << endl << g.size() << endl;

  for (i = 0; i < (int)g.size(); i++)
  {
    {
      s << "##### Gene tree " << i << endl;
      s << g[i]->nnicost << " " << g[i]->nnicost.mut() << endl;
      s << (*g[i]) << endl;
      s << "mdup "; for (j = 0; j < nn; j++) s << mdup[i][j] << " ";
      s << endl;
      s << "mspec "; for (j = 0; j < nn; j++) s << mspec[i][j] << " ";
      s << endl;
      s << "mout "; for (j = 0; j < nn; j++) s << mout[i][j] << " ";
      s << endl;
      s << "#Species tree vs gene tree structs" << endl;
      _nniprint(string(""), *g[i], i, s);
    }
  }
  s.close();
}

void savelog(string s)
{
  ofstream myfile;
  myfile.open ("fasturec.log", ios::app);
  time_t rawtime;
  time ( &rawtime );
  myfile << "#" << ctime (&rawtime) << " ";
  myfile << s << endl;
  myfile.close();
}


void TreeClusters::setlcamappings(RootedTree &s)
{
  map<SPID*, GTCluster*, comparespids>::iterator it;
  for (int i = 0; i < ssize; i++) leaves[i]->lcamap = s.findlab(i);
  int cnt=0;
  for (int i=0; i<internal.size(); i++)
  {
    GTCluster *gc = internal[i];
    //if (gc->lcamap != -1) continue;
    //_setmaps(gc, s);
    if (gc->type<0)
        { SPID old=gc->lcamap;
          gc->lcamap = s.lca( gc->l->lcamap, gc->r->lcamap);
          if (gc->lcamap!=old) cnt++;
      }

    }
  
  //cout << "old=" << cnt<< "/" << internal.size() << endl;
}


void TreeClusters::addgenetrees(GTS &gt)
{
  GTS::iterator gtpos;
  int cnt = 0;
  for (gtpos = gt.begin(); gtpos != gt.end(); ++gtpos)
    (*gtpos)->setspclusters(this);
}

GTCluster* TreeClusters::get(GTCluster *l, GTCluster *r)
/*
Returns a cluster from two children clusters l and r.
*/
{

  GTCluster *gc;
  
  SPID *s = joinspclusters(l->spcluster, r->spcluster); // OK

  if (s[0]==1) return t[s]; // leaf cluster

  map<SPID*, GTCluster*, comparespids>::iterator it = t.find(s);

  if (it == t.end()) {
    // add new cluster 
    gc = new GTCluster(l, r, s);
    t[s] = gc;
    internal.push_back(gc);
  }
  else {
    //cluster present

    gc = t[s];
    deletespcluster(s); 
  }

  gc->usagecnt++;
  _usagecnt++;
  return gc;
}

SPID TreeClusters::_setmaps(GTCluster *gc, RootedTree &s)
{
  if (gc->lcamap != -1) return gc->lcamap;
  return gc->lcamap = s.lca(_setmaps(gc->l, s), _setmaps(gc->r, s));
}


inline bool isInteger(const std::string & s)
{
   if(s.empty() || ((!isdigit(s[0])) && (s[0] != '-') && (s[0] != '+'))) return false ;

   char * p ;
   strtol(s.c_str(), &p, 10);

   return (*p == 0) ;
}


RootedTree *randspeciestree()
{
  SPID tr[specnames.size()];
  for (int i = 0; i < (int)specnames.size(); i++) tr[i] = i;
  return new RootedTree(strdup(genrandomtree(tr, specnames.size()).c_str()));
}

void insertstrees(vector<char*> &v,const char *t)
{
  char *x, *y = strdup(t);
  x=y;

  while (strlen(x)>0)
  {
    char *p=strchr(x,';');
    if (p)
    {      
      *p=0;      
      v.push_back(strdup(x));
      x=p+1;
    }
    else
    {
      v.push_back(strdup(x));      
      break;
    }
  }
  free(y);
}

int main(int argc, char **argv) {
  int opt;

  if (argc < 2) usage(argc, argv);
  STS stvec;
  GTS gtvec;

  struct timeval time; 
  gettimeofday(&time,NULL);
     
  srand((time.tv_sec * 1000) + (time.tv_usec / 1000));
  
  int costtype = COSTDL;
  string sarg = "";
  int linetree = 0;

  int jopt = 0;

  int genopt = 0;
  int nniopt = 0;

  int OPT_PRINTGENE = 0;
  int OPT_PRINTSPECIES = 0;
  int OPT_DEBUG = 0;
  int OPT_GENCONSONLY = 0;
  int OPT_RUNHEURISTIC = 0;
    
  int OPT_UNROOTEDRECONCILIATION = 0;
  int OPT_RECDETAILS = 0;
  int OPT_RECMINROOTING = 0;
  int OPT_RECMINCOST = 0;
  int OPT_SUMMARYTOTAL = 0;
  int OPT_SUMMARYDLTOTAL = 0;
  int OPT_RECPF = 0;
  int OPT_RECST = 0;
  int OPT_PRINTROOTINGS = 0;
  int OPT_RUNSTARHEUR = 0;
  int OPT_PRINTINFO=0;
  int OPT_COMPRESSDUP=1;
  int OPT_USERSPTREEISSTARTING=1;
  int OPT_RUNHEURTEST=0;
  int OPT_PRESERVEROOT=0;
  int OPT_PRESERVEROOTSKIPFIRST=0;
  int OPT_SHOWSTARTINGTREES=0;
  string heuristicalg = "";

  //const char* optstring = "a:T:g:s:S:G:pPYl:xbaAL:D:RoOX:yck:t:N:u:ZJr:j:Q:q:w:iMC";

  const char* optstring = "e:g:s:S:G:T:l:w:H:YMZEk:j:r:q:Q:u:L:D:X:v:c:";
  vector<char*> sgtvec, sstvec;

  while ((opt = getopt(argc, argv, optstring))   != -1)
  {
    sarg = sarg + string(" -") + (char)opt + " ";

    switch (opt) {

      case 'e':
        if (strchr(optarg,'C')) OPT_COMPRESSDUP=0;              
        if (strchr(optarg,'c')) OPT_PRINTINFO=1;
        if (strchr(optarg,'g')) OPT_PRINTGENE = 1;
        if (strchr(optarg,'s')) OPT_PRINTSPECIES = 1;
        if (strchr(optarg,'x')) OPT_DEBUG = 1;
        if (strchr(optarg,'R')) OPT_PRINTROOTINGS = 1;
        if (strchr(optarg,'a')) savetabstyle = ARR_SIMPLE;
        if (strchr(optarg,'A')) savetabstyle = ARR_DATE;
        if (strchr(optarg,'i')) OPT_USERSPTREEISSTARTING=0;
        if (strchr(optarg,'r')) OPT_PRESERVEROOT=1;
        if (strchr(optarg,'0')) OPT_PRESERVEROOTSKIPFIRST=1;
        if (strchr(optarg,'S')) OPT_SHOWSTARTINGTREES=1;
        break;
    
    case 'v': 
      verbose=atoi(optarg);
      break;

    case 'c': 
      inferredspeciesrteescount = atoi(optarg);
      break;

    case 'g':
    
        insertstrees(sgtvec,optarg);        
        sarg += quoted(optarg);
        break;
    

    case 'H':
      heuristicalg=strdup(optarg);
      sarg += quoted(optarg);
      break;

    case 's':
      insertstrees(sstvec,optarg);              
      sarg += quoted(optarg);
      break;
    case 'S':
      if (strstr(optarg, ".raw"))
        sstvec.push_back(strdup(optarg)); // raw tree
      else
        readtrees(optarg, sstvec);
      sarg += optarg;
      break;
    case 'G': {
      readtrees(optarg, sgtvec);
      string oa(optarg);      
      string bf = oa.substr(oa.find_last_of("/\\") + 1);      
      if (bf.find_last_of(".")!=string::npos)
        if (bf.substr(0,bf.find_last_of("."))==".txt")
          bf=bf.substr(0,bf.find_last_of("."));
      projectname=strdup(bf.c_str());
      sarg += optarg;
      break;
    }

    case 'T':
      readtreesinterleaved(optarg, sgtvec, sstvec);
      sarg += optarg;
      break;

    case 'l':
    {
      switch (optarg[0])
      {
      case 'p':
        gspos = atoi(optarg + 1);
        gsid = GSPOS;
        break;
      case 'a':
        gsid = GSAFTER;
        gsdelim = strdup(optarg + 1);
        break;
      case 'b':
        gsid = GSBEFORE;
        gsdelim = strdup(optarg + 1);
        break;
      default:
        cerr << "Invalid code for -l option expected aDELIM,bDELIM or p[-]number" << endl;
        exit(-1);
      }
      break;
    }


    case 'w':
      OPT_GENCONSONLY = 1;
      if (sscanf(optarg, "%d", &nniconstrees) != 1) {
        cerr << "Number expected in -w" << endl;
        exit(-1);
      }
      sarg += optarg;
      break;

    case 'Y':    
      OPT_RUNSTARHEUR = 1;
      break;

    case 'M':      
      heuristicalg=strdup("NS1NS1NS1NS1T5");
      break;

    case 'Z':
      heuristicalg=strdup("MNS5T5");
      break;
    
    
    case 'j':
      if (sscanf(optarg, "%d", &mixexplorebigsteps) != 1) {
        cerr << "Number expected in -j" << endl;
        exit(-1);
      }
      break;

    case 'r':
      if (sscanf(optarg, "%d", &nnirandomtrees) != 1) {
        cerr << "Number expected in -r" << endl;
        exit(-1);
      }
      break;

    case 'q':
      if (sscanf(optarg, "%d", &nniconstrees) != 1) {
        cerr << "Number expected in -q" << endl;
        exit(-1);
      }
      break;


    case 'Q':
      if (sscanf(optarg, "%lf", &nniconsprob) != 1) {
        cerr << "Number expected in -Q" << endl;
        exit(-1);
      }
      break;

    case 'u':
      OPT_UNROOTEDRECONCILIATION = 1;
      if (strchr(optarg, 'd')) OPT_RECDETAILS = 1;
      if (strchr(optarg, 's')) OPT_RECST = 1;
      break;


    case 'L':
      sarg += optarg;
      if (sscanf(optarg, "%lf", &weight_loss) != 1) {
        cerr << "Number expected in -L" << endl;
        exit(-1);
      }
      break;

    case 'D':
      sarg += optarg;
      if (sscanf(optarg, "%lf", &weight_dup) != 1) {
        cerr << "Number expected in -D" << endl;
        exit(-1);
      }
      break;



    case 'X':
    {
      OPT_RECPF = 1;
      sarg += optarg;
      linetree = 0;
      char *oa = optarg;
      if (optarg[0] == 'l') {
        linetree = 1;
        oa++;
      }
      costtype = -1;
      if (!strcmp(oa, "D")) costtype = COSTD;
      if (!strcmp(oa, "L")) costtype = COSTL;
      if (!strcmp(oa, "DC")) costtype = COSTDC;
      if (!strcmp(oa, "T")) costtype = COSTTEST;
      if (!strcmp(oa, "DL")) costtype = COSTDL;
      if (!strcmp(oa, "RF")) costtype = COSTRF;
      if (!strcmp(oa, "RFFD")) costtype = COSTRFFD;
      if (!strcmp(oa, "RFO")) costtype = COSTRFO;
      if (!strcmp(oa, "S")) costtype = COSTS;
      if (costtype < 0)
      {
        cerr << "Unknown cost type" << endl;
        exit(-1);
      }
      break;
    }

    default:
      cerr << "Unknown option: " << ((char) opt) << endl;
      exit(-1);
    }

  } // for

  // cout << sarg << endl;

#ifdef ARGSLOG
  savelog(sarg);
#endif

  verb_smp << "Number of gene trees: " << sgtvec.size() << endl;

  for (int i = 0; i < sstvec.size(); i++)
    stvec.push_back(new RootedTree(sstvec[i]));

  if (OPT_COMPRESSDUP)
  {
      verb_det << "Compressing duplications..." << endl;
  }
  int totalcomp=0;
  int gtrees=0;
  int toosmall=0;
  for (int i = 0; i < sgtvec.size(); i++)
  {
     UnrootedTree *g = new UnrootedTree(sgtvec[i]);
     if (g->lf==1 || g->lf==2)
     {  toosmall++;
        delete g;
        continue;
     }
     
     int dup=0;
     if (OPT_COMPRESSDUP) dup = g->compressduplications();     
    
     if (dup)
        {
           totalcomp+=dup;
           std::stringstream ss;
           g->print(ss);           
           gtrees++;
           g=new UnrootedTree(strdup(ss.str().c_str()),dup);              
        }  
      gtvec.push_back(g);

  } // for

  if (gtrees)
    verb_smp << "I've found " << totalcomp << " constant duplication(s) in " << gtrees << " gene trees. Gene trees are now compressed." << endl;

  if (toosmall)
    verb_smp << "Warning: " << toosmall << " gene tree(s) of size 1 or 2 - ignored" << endl;

  if (OPT_PRINTINFO)
    {
      for (int i=0;i<specnames2id.size();i++)
	     cout << i << " " << specnames[i] << endl;
    }
  
  STS::iterator stpos;
  GTS::iterator gtpos;

  if (OPT_PRINTSPECIES)
    for (stpos = stvec.begin(); stpos != stvec.end(); ++stpos)
      (*stpos)->print(cout) << endl;

  if (OPT_PRINTGENE)
    for (gtpos = gtvec.begin(); gtpos != gtvec.end(); ++gtpos)    
      (*gtpos)->print(cout) << endl;
    


  if (OPT_RECPF) {
    for (stpos = stvec.begin(); stpos != stvec.end(); ++stpos)
      for (gtpos = gtvec.begin(); gtpos != gtvec.end(); ++gtpos)
      {
        RootedTree *s = *stpos;
        s->setdepth();
        UnrootedTree *g = *gtpos;
        g->clean();
        g->setlcamappings(*s);
        SPID un = g->findoptimal(*s);
        g->mark(g->par[un], 2 | 8);
        g->mark(un, 2);
        g->pf(cout, *s, costtype, linetree);
        cout << endl;
      }
  }

  if (OPT_UNROOTEDRECONCILIATION) {
    if (totalcomp)
      cout << "Compressed duplication score: " << totalcomp << endl;
    int num = 0;
    int stnum = 1;
    for (stpos = stvec.begin(); stpos != stvec.end(); ++stpos) {
      RootedTree *s = *stpos;
      s->setdepth();
      if (OPT_RECDETAILS)
      {
        cout << "Species tree " << stnum << ": " << *s << endl;
        if (OPT_DEBUG) s->printdeb(cout,1) << endl;
      }

      DlCost total;

      for (gtpos = gtvec.begin(); gtpos != gtvec.end(); ++gtpos, num++)
      {
        UnrootedTree *g = *gtpos;
        g->clean();
        g->setlcamappings(*s);

        if (OPT_DEBUG) g->printdeb(cout,0) << endl;

        SPID un = -1;
        un = g->findoptimal(*s);

        if (OPT_RECDETAILS)
        {
          DlCost gc = g->dlcost(*s, un);
          cout << gc << "\t" << gc.mut() << "\t";
          g->printrooting(cout, un) << endl;
        }

        DlCost s1 = g->dlcost(*s, un);
        total.loss += s1.loss;
        total.dup += s1.dup;
      } // gt-loop

      if (OPT_RECDETAILS)
      {
        cout << "Total cost:";
        cout << total << "\t" << total.mut() << "\t" << endl << endl;
        stnum++;
      }
      else {
        if (OPT_RECST)
          cout << total << "\t" << total.mut() << "\t" << *s << endl;
        else
          cout << total.mut() << endl;
      }


    } // st-loop
  } // (OPT_UNROOTEDRECONCILIATION)


  gtc = new TreeClusters();
  gtc->addgenetrees(gtvec);
  verb_det << "LCA cluster structures completed. Found " << gtc->size() << " items. Total usage cnt " << gtc->usagecnt() << endl;  


  if (OPT_GENCONSONLY)
  {

    RootedTree *sr=NULL;
    if (OPT_PRESERVEROOT)
    {
      if (stvec.size()>0)
          sr=stvec[0];
      else 
      {
          cout << "Exactly one species tree should be defined for quasi-consensus trees with preserve-root option" << endl;
          exit(2);        
      }
    }
      
    if (!gtvec.size())
    {
      cout << "Empty collection of gene trees - quasi consensus not found" << endl;
      return -1;
    }

    RootedTree *r;
    for (int i = 0; i < nniconstrees; i++)
    {
      
      r = gtc->genrooted(sr);
      if (!r)
      {
        cerr << "Cannot create initial species tree" << endl;
        exit(-1);
      }

      cout << *r << endl;
    }
    return 0;
  }


  if (heuristicalg.size() && gtvec.size())
  {    
    verb_smp << "Inferring starting point species trees" << endl;

    if (!OPT_USERSPTREEISSTARTING and stvec.size())
    {
      verb_smp << "User defined trees removed from the list of starting points" << endl;      
      stvec.clear();
    }

    if (stvec.size())        
      verb_smp << "Starting points: user defined " << stvec.size() << " sp. tree(s)" << endl;
    
    // Gen initial sp. trees
    if (!nniconstrees && !nnirandomtrees && !stvec.size()) nniconstrees = 1;
    
    RootedTree *r;
    // Gen quasi consensus trees
    if (nniconstrees)
      verb_smp << "Starting points: " << nniconstrees << " quasi-consensus species tree(s)" << endl;

      
    for (int i = 0; i < nniconstrees; i++)
    {
      
      RootedTree *gst=NULL;
      if (OPT_PRESERVEROOT) gst=stvec[0];
      if (OPT_PRESERVEROOTSKIPFIRST)
        stvec.erase(stvec.begin()); // Remove the first 

      r = gtc->genrooted(gst);
      if (!r)
      {
        cerr << "Cannot create initial species tree" << endl;
        exit(-1);
      }
      stvec.push_back(r);

    }



    // Gen random trees
    if (nnirandomtrees)
      verb_smp << "Starting points: " << nnirandomtrees << " random sp. tree(s)" << endl;    
    for (int i = 0; i < nnirandomtrees; i++)
    {
      r = randspeciestree();
      if (!r)
      {
        cerr << "Cannot create initial random species tree" << endl;
        exit(-1);
      }
      stvec.push_back(r);
    }        

    if (OPT_SHOWSTARTINGTREES)
    {
      cout << "Starting trees:" << endl;
      for (stpos = stvec.begin(); stpos != stvec.end(); ++stpos)
        (*stpos)->print(cout) << endl;

    }

    verb_smp << "Heuristic algorithm: " << heuristicalg << endl;
    
    vector<LocalSearch*> t;
    int enginemulti=0;
    int p = 0;

    while (p<heuristicalg.size())
    {      
        char tp = heuristicalg[p];

        if (isspace(tp)) { p++; continue; }
        
        if (!strchr("MNST",tp))
        {
          cout << "Expected type [MNST] of heuristic step in -H option" << endl;
          exit(-3);
        }        
        p+=1;                
        int v=0;
        while (isdigit(heuristicalg[p])) 
          { v=v*10+(int(heuristicalg[p])-48); p++; }
        
        LocalSearch *ls=NULL; 
        if (tp=='N') ls=new LocalSearchNNI(v);
        else if (tp=='S') ls=new LocalSearchSPRN(v);
        else if (tp=='T') ls=new LocalSearchTSW(v);
        else if (tp=='M') enginemulti=1;

        if (tp!='M')
        {
          verb_smp << "Building heuristic: " << ls->name() << " repeat=" << v << endl;
          t.push_back(ls);
        }           
    }

    if (enginemulti) 
    { 
      verb_smp << "Starting multiple queues algorithm with " << stvec.size() << " queue(s)" << endl;
      mixexploremulti(stvec, gtvec,t, 1, OPT_PRESERVEROOT);
    }
    else 
    { 
      verb_smp << "Starting single queue algorithm" << endl;
      mixexplore(stvec, gtvec, t, 1, OPT_PRESERVEROOT);
    }    
  }


  if (OPT_RUNSTARHEUR && gtvec.size())
  {
      verb_smp << "Running STARHEUR" << endl;
      vector<LocalSearch*> t;
      t.push_back(new LocalSearchNNI(-1));
      //t.push_back(new LocalSearchSPRDist(gtvec,-1,5));
      //t.push_back(new LocalSearchTSW(gtvec,-1));
      t.push_back(new LocalSearchSPRN(3));
      mixexplorebigsteps=1;
      runstarheur(gtvec,t, OPT_PRESERVEROOT);
  }


  // Ignore heuristics if no trees are given
  if ((heuristicalg.size() || OPT_RUNSTARHEUR) && !gtvec.size())    
      cerr << "No gene tree is defined. Option -G or -g expected." << endl;                
    

  // Clean 
    
  for (int i = 0; i < sgtvec.size(); i++) 
    free(sgtvec[i]);


  for (int i = 0; i < sstvec.size(); i++) 
    free(sstvec[i]);

  for (int i = 0; i < stvec.size(); i++) 
    delete stvec[i];

  delete gtc; // Clean TreeClusters

  for (gtpos = gtvec.begin(); gtpos != gtvec.end(); ++gtpos)    
      delete *gtpos;

  cleanspecies();

  if (projectname) free(projectname);
}

