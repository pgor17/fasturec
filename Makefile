CPPFLAGS = -O3
CC = g++ 
LFLAGS =  

all: fasturec

tools.o: tools.cpp tools.h

fasturec: fasturec.o tools.o

gdb: clean  
	g++ -g -o fasturec tools.cpp fasturec.cpp 

clean:
	rm -f *.o $(TARGET) *.old *~ x *.log

o1g: clean 
	g++ -g -O1 -o fasturec tools.cpp fasturec.cpp 

o3: clean 
	g++ -g -O3 -o fasturec tools.cpp fasturec.cpp 
	
tgz : 
	tar czvf fasturec.tgz fasturec.cpp tools.h tools.cpp Makefile datasets/

tgzlin : 
	tar czvf fasturec2.tgz fasturec2 README g3simple.txt ac.txt

check-syntax:
	gcc -o nul -S ${CHK_SOURCES}
