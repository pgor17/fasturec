
PATH=$PATH:.

rm -f test_fu.log

for h in Y Z M
do 	
	for l in "g3 DL 64413 L1" "g3 D 15822 L0" "ac DL 7188 L1" "ac D 2240 L0"
	do
		set $l 
		f=$1
		cfun=$2
		optcost=$3
		loss=$4
		rm fu.txt
		fasturec -$h -G datasets/$1.txt -ea -$loss

		RES=$( head -1 fu.txt | cut -f1 -d' ' )
		if [[ $RES = $optcost ]]
		then
			echo -e "$f.txt\t$cfun\t$optcost\t-$h\tOK"
		else
			echo -e "$f.txt\t$cfun\t$optcost\t-$h\tSuboptimal $RES"		
		fi >> test_fu.log

	done
done

echo 
echo "Summary"
echo -e "Dataset\tCostFun\tOptimal\tHeurOpt\tFasturec_outcome"

cat test_fu.log

