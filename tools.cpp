

/************************************************************************
 Unrooted REConciliation version 1.00
 (c) Copyright 2005-2006 by Pawel Gorecki
 Written by P.Gorecki.
 Permission is granted to copy and use this program provided no fee is
 charged for it and provided that this copyright notice is not removed.
 *************************************************************************/

#include <ctype.h>
#include <iostream>
#include <cstdlib>
#include <cstring>

using namespace std;

#include "tools.h"

vector<string> specnames;
vector<int> specorder;
map<string, int> specnames2id;
map<SPID, SPID*> spec2spcluster;

int linenum = 0;
int charnum = 0;
char *inputfilename = (char*)NULL;

SPID topspcluster[2] = {0, 0};

void printspcluster(ostream&s, SPID *a) {
  s << a[0] << ":";
  for (int i = 1; i <= a[0]; i++) s << a[i] << " ";
}
  

SPID* joinspclusters(SPID* a, SPID* b, SPID *dest)
{
  if (!a[0]) return a;
  if (!b[0]) return b;

  int as = a[0], bs = b[0], cs = 1;  
  
  SPID r[as+bs+1];

  int i = 1, j = 1;

  while (i <= as && j <= bs)
    if (a[i] == b[j]) { r[cs++] = a[i];  i++; j++; }
    else if (a[i] < b[j]) r[cs++] = a[i++];
    else if (a[i] > b[j]) r[cs++] = b[j++];

  while (i <= as) r[cs++] = a[i++];
  while (j <= bs) r[cs++] = b[j++];

  if (cs == 2) 
    return spec2spcluster[r[1]];

  if (cs - 1 == specnames.size())     
    return topspcluster;

  SPID *rx = dest;
  if (!dest) rx = new SPID[cs];

  for (i=1; i<cs; i++) rx[i] = r[i];
  rx[0] = cs - 1;

  return rx;
}


bool spsubseteq(SPID *a, SPID *b)
{
  if (b==topspcluster) return 1;
  if (a==topspcluster) return b==topspcluster;
  if (spsize(a)>spsize(b)) return false;

  int jp=1;
  for (int i = 1; i <= a[0]; i++)
  {
    while (a[i]>b[jp]) 
      { jp++; 
        if (jp>b[0]) return false;  //last
      } // not found
    if (a[i]<b[jp]) return false;
  }
  return true;
}

void initlinenuminfo(char *fname)
{
  linenum = 1;
  charnum = 0;
  inputfilename = fname;
}

SPID* spidcopy(SPID* a, int size)
{
  SPID *res = new SPID[size];
  for (int i = 0; i < size; i++) res[i] = a[i];
  return res;
}

void printlinepos()
{
  if (inputfilename) cerr << inputfilename << ":";
  cerr << linenum << ":" << charnum << ": ";
}

void expectTok(const char *tok, char *s, int p)
{
  printlinepos();
  cerr << "parse error - expected #" << tok << "#" << " after " << endl;
  int x = p;
  if (x < 70) x = 0;
  else { x -= 70; cerr << "..."; }
  while (s[x] && x < p + 70)
  {
    if (x == p) cerr << endl << "Found: " << endl;
    cerr << s[x++];
  }
  if (s[x]) cerr << "...";
  cerr << endl;
  exit(-1);
}

char* getTok(char *s, int &p, int num)
{
#define inctok { p++; if (s[p]=='\n') { linenum++; charnum=0; } charnum++; }
  while (isspace(s[p])) inctok;

  char *cur = s + p;
  if (isalnum(s[p]) || (s[p] == '_'))
  {
    while (isalnum(s[p]) || (s[p] == '_') || (s[p] == '.') || (s[p] == '-')) inctok;
    return cur;
  }
  if ((s[p] == '(')  || (s[p] == ')') || (s[p] == ',') || s[p] == ':')
  {
    inctok;
    return cur;
  }
  if (num >= 0) cerr << "Tree nr " << num << ". ";
  printlinepos();
  cerr << " parse error - expected expected LABEL, '(', ')' or ','; found: '" << s[p]
       << "'. current string: <<" << cur << ">>" << endl;
  exit(-1);
}


void checkparsing(char *s)
{
  int p = 0;
  while (s[p])
  {
    if (s[p] == ';') { inctok; continue; }
    if (!isspace(s[p]))
    {
      printlinepos();
      cerr << "EOLN, EOF or ';' expected, found '" << (s + p) << "'" << endl;
      exit(-1);
    }
    inctok;
  }
}

int getspecies(char *s, int len)
{

  char old;
  int num;
  if (len)
  {
    old = s[len];
    s[len] = 0;
  }
  
  if (!specnames2id.count(s))
  {
    specnames2id[s] = num = specnames.size();
    specorder.push_back(num);
    specnames.push_back(s);    
    SPID* speccluster = new SPID[2];
    speccluster[0] = 1;
    speccluster[1] = num;
    spec2spcluster[num] = speccluster;
  }
  else num = specnames2id[s];

  if (len) s[len] = old;

  return num;
}

void cleanspecies()
{
    int i;
    for (SPID i = 0; i < specnames.size(); i++)
      delete[] spec2spcluster[i];
}


char *mstrndup(const char *s, size_t n)
{
  char *result;
  size_t len = strlen (s);
  if (n < len) len = n;
  result = (char *) malloc (len + 1);
  if (!result) return 0;
  result[len] = '\0';
  return (char *) memcpy (result, s, len);
}


void randomizearr(SPID *t, int size)
{
  for (int i = 0; i < size; i++)
  {
    int np = rand() % size;
    int tmp = t[i];
    t[i] = t[np];
    t[np] = tmp;
  }
}


extern const char *FASTUREC;

int usage(int argc, char **argv) {

  cout <<
       "FAST-Unrooted Reconciliation and supEr-tree inferenCe" << FASTUREC << " (C) P.Gorecki 2005-2021\n"
       "Software homepage: https://bitbucket.org/pgor17/fasturec\n"
       "Usage: " << argv[0] << " [options]\n\n"
       "INPUT OPTIONS:\n"
       "  -g gene tree\n"
       "  -s species tree\n"
       "  -G filename - a set of gene trees\n"
       "  -S filename - a set of species trees\n"
       "  -T filename - joint format - gene trees + one species tree in the last line\n"

       "  -l [aDELIM|bDELIM|pPOS] - rules for matching gene tree names with species tree names;\n"
       "       a - after delimiter; b - before delimiter; p - from position \n"
       "\n"
       "ADDITIONAL OPTIONS\n"
       "  -v NUM - verbose level 1 (print the name of outputfile only),\n"
       "       2 (simple output), 3 (detailed output)\n"
       "  -w NUM - print NUM quasi-consensus species trees and exit\n"       
       "  -e [gsCRaAir]+ - multiple options:\n"
       "     g - print a gene tree\n"
       "     s - print a species tree\n"
       "     C - do not compress constant duplication nodes\n"
       "     R - print all rootings of gene trees\n"
       "     a - output filename fu.txt\n"
       "     A - output filename <currentdatetime>.fu.txt\n"
       "     i - remove user species trees (see -s,-S) from the list of heuristic starting points\n"
       "     r - preserve the root from the first species tree (not available in star heuristic); see -e0\n"
       "     0 - to be used with r; the first species tree is not initial in the heuristic\n"
       "     I - show all starting trees\n"              
       "\n"       
       "COST SETTING OPTIONS\n"
       "  -D dupweight  - set weight of gene duplications (def. 1.0)\n"
       "  -L lossweight - set weight of gene losses (def. 1.0)\n"
       "  -X COST - print extended notation for star-like topologies under COST from {DL,D,S,DC,RF,L}\n"
       "\n"

       "UNROOTED RECONCILIATION\n"
       "  -u [xsd] - unrooted reconciliation - gene trees vs species trees\n"
       "    x - print total costs only for every species tree\n"
       "    s - print total costs with species trees\n"
       "    d - detailed information for every pair of trees; one optimal rooting is shown\n"       
       "\n"


       "HEURISTIC SEARCH\n"
       
       //  " -y - show branch lengths (preserved during unrooted -> rooted convertion)" << endl;
       //   " -u - cluster stats operations (s - save file, r - read file not implemented yet)" << endl;       
       
       "*Starting tree options:\n"
       "  -q NUM - generate NUM quasi-consensus trees sampled from gene tree clusters;\n"
       "       (def. is 1 if no user species tree is defined)\n"
       "  -r NUM - generate NUM random trees (def. 0)\n"
       "  -Q p - defines probability for sampling gene trees (use with -q, default 0.2)\n"
       "  See also -wNUM, -ei and -er.\n"
       "*Heuristic methods\n"
       " -H (N[NUM]|S[NUM]|T[NUM]|M)+ - defines local search combination for heuristic:\n"
       "     N[NUM] - NNI local search\n"
       "     S[NUM] - SPR local search\n"
       "     T[NUM] - subtree swapping local search\n"
       "     M - use multiple queue method (one queue per each starting tree)\n"
       "     NUM in N,S,T denotes the number of the corresponding single search types in every loop\n"
       "     lack of NUM denotes searching until no improvement is found\n"
       " -Y - star heuristic (uses NNI and SPRn for improving costs)\n"
       " -Z - equivalent to -H MNS5T5\n"
       " -M - equivalent to -H NS1NS1NS1NS1T5\n"
       
       "*Other heuristic parameters\n"
       " -j steps - defines the number tries with waiting queue trees after locating\n"
       "       a new localy optimal tree (def. 2); only for single queue method\n";

//    "-X COST - show gene tree with plateau and costs where COST (-b required): " << endl <<
//     "   DL, D, S, DC, RF, L";
//    " For every reconciliation of an unrooted gene tree with a species tree (details of costs):"
//        << endl;
//    "   -o - show an optimal cost" << endl;
//    "   -O - show an optimal rooting" << endl;
//    "   -a - show attributes and mappings" << endl;
//    "   -A - show detailed attributes" << endl;
//   cout
//     << " For every species tree, i.e., summary of costs when reconciling a species tree with a set of gene trees):"
//     << endl;
//    "   -c - print total mutation cost" << endl;
//    "   -C - print total dl-cost (dup,loss)" << endl;
//    "   -d - print detailed total cost (distributions)" << endl;
//    "   -x - print species tree with detailed total costs (nested parenthesis notation with attributes)"
//        << endl;

//    "   -N - print nni variants for rooted (use -s/-S) or unrooted (-g/G) species trees" << endl;
//    " -Mg - graphviz output, example: " << endl;
//    "          urec -Mg -G gtree.txt -p | dot -Tps > gtree.ps"  << endl;

  exit(-1);
}



// return random tree string from given vector of species
string genrandomtree(SPID *sp, int len)
{
  vector<string> v;
  int i;
  for (i = 0; i < len; i++)
    v.push_back(species(sp[i]));

  while (v.size() > 1)
  {
    int a = rand() % v.size();
    string as = v[a];
    v.erase(v.begin() + a);

    int b = rand() % v.size();
    string bs = v[b];
    v.erase(v.begin() + b);

    v.push_back("(" + as + "," + bs + ")");
  }
  return v[0];
}